# Copyright (c) 2022 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""tools unit tests."""

import sys
import unittest

from unittest.mock import MagicMock, patch, mock_open

from requests import Response, exceptions

import yaml

from opentf.tools.ready import _find

import opentf.tools.ready


########################################################################
# Datasets

INIT_SERVICES_OK = {
    "services": [
        "postman",
        "inceptionee",
        "Observer",
        "localpublisher",
        "skf",
        "LocalCleaner",
        "cucumber.interpreter",
        "surefire.interpreter",
        "agentchannel",
        "result.aggregator",
        "rewriter",
        "cypress",
        "postman.interpreter",
        "allure.collector",
        "soapui",
        "robotframework",
        "tm.publisher",
        "Arranger",
        "robot.interpreter",
        "actionprovider",
        "dummyee",
        "junit",
        "Killswitch",
        "cypress.interpreter",
        "soapui.interpreter",
        "Receptionist",
        "s3publisher",
        "tm.generator",
        "sshchannel",
        "cucumber",
    ],
    "checksum": "329ab8973c3474f565ecb9bd2d443c7d2fb45aef986c0e172efabccb40c2d948",
}

INIT_SERVICES_BAD = {
    "services": [
        "postman",
        "inceptionee",
        "Observer",
        "tm.generator",
        "sshchannel",
        "cucumber",
        "foobar",
    ],
    "checksum": "329ab8973c3474f565ecb9bd2d443c7d2fb45aef986c0e172efabccb40c2d948",
}

SUBSCRIBERS_OK = [
    'rewriter',
    'junit',
    'inceptionee',
    'result.aggregator.workflow-completed',
    'actionprovider',
    'robotframework',
    'arranger',
    'cucumber.interpreter',
    'result.aggregator.execution-result',
    'result.aggregator.execution-command',
    'cypress.interpreter',
    'postman',
    'localpublisher',
    'ranorex',
    'dummyee',
    'soapui.interpreter',
    'postman.interpreter',
    'skf',
    'localcleaner',
    'result.aggregator.workflow-canceled',
    's3publisher',
    'uft',
    'tm.publisher',
    'cucumber',
    'cypress',
    'tm.generator.premium',
    'agilitest',
    'uft.interpreter',
    'ranorex.interpreter',
    'agilitest.interpreter',
    'allure.collector',
    'sshchannel',
    'surefire.interpreter',
    'robot.interpreter',
    'observer',
    'soapui',
    'agentchannel',
]

SUBSCRIPTIONS_OK = {
    'apiVersion': 'v1',
    'items': {
        '0367d855-43d9-4ab0-9236-d0482b18b050': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ExecutionResult',
                    'opentestfactory.org/labelselector': '',
                },
                'creationTimestamp': '2022-01-06T11:20:47.501304',
                'name': 'tm.publisher',
            },
            'spec': {
                'selector': {'matchKind': 'ExecutionResult'},
                'subscriber': {
                    'endpoint': 'http://127.0.0.1:10066/publisher/inbox/executionResult'
                },
            },
            'status': {
                'lastPublicationTimestamp': '2022-01-06T11:54:01.531123',
                'publicationCount': 21,
                'publicationStatusSummary': {'204': 21},
            },
        },
        '051d1803-2bca-4839-b3fe-66d8fd773c65': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ReportInterpreterInput',
                    'opentestfactory.org/labelselector': '',
                },
                'creationTimestamp': '2022-01-06T11:20:41.171408',
                'name': 'ranorex.interpreter',
            },
            'spec': {
                'selector': {'matchKind': 'ReportInterpreterInput'},
                'subscriber': {
                    'endpoint': 'http://127.0.0.1:10087/ranorexReportInterpreter/inbox'
                },
            },
            'status': {
                'lastPublicationTimestamp': '2022-01-06T11:54:10.429826',
                'publicationCount': 2,
                'publicationStatusSummary': {'204': 2},
            },
        },
        '0d2c2b5e-dbc0-424c-9ae9-2c3df9a3c66a': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ProviderCommand,apiVersion==opentestfactory.org/v1alpha1',
                    'opentestfactory.org/labelselector': 'opentestfactory.org/category==params,opentestfactory.org/categoryPrefix==cucumber5,opentestfactory.org/categoryVersion==v1',
                },
                'creationTimestamp': '2022-01-06T11:17:48.423892',
                'name': 'cucumber',
            },
            'spec': {
                'selector': {
                    'matchFields': {'apiVersion': 'opentestfactory.org/v1alpha1'},
                    'matchKind': 'ProviderCommand',
                    'matchLabels': {
                        'opentestfactory.org/category': 'params',
                        'opentestfactory.org/categoryPrefix': 'cucumber5',
                        'opentestfactory.org/categoryVersion': 'v1',
                    },
                },
                'subscriber': {'endpoint': 'http://127.0.0.1:7784/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': None,
                'publicationCount': 0,
                'publicationStatusSummary': {},
            },
        },
        '0f1b9ce0-0f77-482e-b2ae-1a6c4e470f39': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==GeneratorResult,apiVersion==opentestfactory.org/v1alpha1',
                    'opentestfactory.org/labelselector': '',
                },
                'creationTimestamp': '2022-01-06T11:17:47.386899',
                'name': 'observer',
            },
            'spec': {
                'selector': {
                    'matchFields': {'apiVersion': 'opentestfactory.org/v1alpha1'},
                    'matchKind': 'GeneratorResult',
                },
                'subscriber': {'endpoint': 'http://0.0.0.0:7775/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': None,
                'publicationCount': 0,
                'publicationStatusSummary': {},
            },
        },
        '10c1aed0-2b00-4768-83bc-276f729d905c': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ProviderCommand',
                    'opentestfactory.org/labelselector': 'opentestfactory.org/category==prepare-inception,opentestfactory.org/categoryPrefix==actions',
                },
                'creationTimestamp': '2022-01-06T11:17:48.600293',
                'name': 'actionprovider',
            },
            'spec': {
                'selector': {
                    'matchKind': 'ProviderCommand',
                    'matchLabels': {
                        'opentestfactory.org/category': 'prepare-inception',
                        'opentestfactory.org/categoryPrefix': 'actions',
                    },
                },
                'subscriber': {'endpoint': 'http://127.0.0.1:7780/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': None,
                'publicationCount': 0,
                'publicationStatusSummary': {},
            },
        },
        '12f667cb-32da-4d11-8bf9-d30c15ac46ac': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ProviderCommand',
                    'opentestfactory.org/labelselector': 'opentestfactory.org/category==checkout,opentestfactory.org/categoryPrefix==actions,opentestfactory.org/categoryVersion==v2',
                },
                'creationTimestamp': '2022-01-06T11:17:48.148411',
                'name': 'actionprovider',
            },
            'spec': {
                'selector': {
                    'matchKind': 'ProviderCommand',
                    'matchLabels': {
                        'opentestfactory.org/category': 'checkout',
                        'opentestfactory.org/categoryPrefix': 'actions',
                        'opentestfactory.org/categoryVersion': 'v2',
                    },
                },
                'subscriber': {'endpoint': 'http://127.0.0.1:7780/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': None,
                'publicationCount': 0,
                'publicationStatusSummary': {},
            },
        },
        '171ff56e-cf5c-4964-808b-ac43c5564cfe': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ProviderCommand',
                    'opentestfactory.org/labelselector': 'opentestfactory.org/category==delete-file,opentestfactory.org/categoryPrefix==actions',
                },
                'creationTimestamp': '2022-01-06T11:17:48.423328',
                'name': 'actionprovider',
            },
            'spec': {
                'selector': {
                    'matchKind': 'ProviderCommand',
                    'matchLabels': {
                        'opentestfactory.org/category': 'delete-file',
                        'opentestfactory.org/categoryPrefix': 'actions',
                    },
                },
                'subscriber': {'endpoint': 'http://127.0.0.1:7780/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': '2022-01-06T11:51:12.376161',
                'publicationCount': 2,
                'publicationStatusSummary': {'200': 2},
            },
        },
        '17fded76-cdcb-4c20-b042-bf49a88ab733': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ProviderCommand',
                    'opentestfactory.org/labelselector': 'opentestfactory.org/category==params,opentestfactory.org/categoryPrefix==uft,opentestfactory.org/categoryVersion==v1',
                },
                'creationTimestamp': '2022-01-06T11:19:39.424622',
                'name': 'uft',
            },
            'spec': {
                'selector': {
                    'matchKind': 'ProviderCommand',
                    'matchLabels': {
                        'opentestfactory.org/category': 'params',
                        'opentestfactory.org/categoryPrefix': 'uft',
                        'opentestfactory.org/categoryVersion': 'v1',
                    },
                },
                'subscriber': {'endpoint': 'http://127.0.0.1:20002/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': None,
                'publicationCount': 0,
                'publicationStatusSummary': {},
            },
        },
        '20877396-f822-4e37-ba88-3e86c6051604': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==WorkflowCanceled',
                    'opentestfactory.org/labelselector': '',
                },
                'creationTimestamp': '2022-01-06T11:20:46.393537',
                'name': 'result.aggregator.workflow-canceled',
            },
            'spec': {
                'selector': {'matchKind': 'WorkflowCanceled'},
                'subscriber': {
                    'endpoint': 'http://127.0.0.1:10088/result-aggregator/inbox/workflow-canceled'
                },
            },
            'status': {
                'lastPublicationTimestamp': '2022-01-06T11:53:56.553301',
                'publicationCount': 1,
                'publicationStatusSummary': {'204': 1},
            },
        },
        '23a1e15b-024e-47e2-b20b-d0efded75cf9': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ProviderCommand,apiVersion==opentestfactory.org/v1alpha1',
                    'opentestfactory.org/labelselector': 'opentestfactory.org/category==cucumber,opentestfactory.org/categoryPrefix==cucumber5,opentestfactory.org/categoryVersion==v1',
                },
                'creationTimestamp': '2022-01-06T11:17:48.514414',
                'name': 'cucumber',
            },
            'spec': {
                'selector': {
                    'matchFields': {'apiVersion': 'opentestfactory.org/v1alpha1'},
                    'matchKind': 'ProviderCommand',
                    'matchLabels': {
                        'opentestfactory.org/category': 'cucumber',
                        'opentestfactory.org/categoryPrefix': 'cucumber5',
                        'opentestfactory.org/categoryVersion': 'v1',
                    },
                },
                'subscriber': {'endpoint': 'http://127.0.0.1:7784/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': None,
                'publicationCount': 0,
                'publicationStatusSummary': {},
            },
        },
        '2561d91d-106d-4107-9303-740d018d47e9': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ReportInterpreterInput',
                    'opentestfactory.org/labelselector': '',
                },
                'creationTimestamp': '2022-01-06T11:20:39.957235',
                'name': 'postman.interpreter',
            },
            'spec': {
                'selector': {'matchKind': 'ReportInterpreterInput'},
                'subscriber': {
                    'endpoint': 'http://127.0.0.1:10092/postmanReportInterpreter/inbox'
                },
            },
            'status': {
                'lastPublicationTimestamp': '2022-01-06T11:54:10.429826',
                'publicationCount': 2,
                'publicationStatusSummary': {'204': 2},
            },
        },
        '2a31046a-152c-4079-bd44-552b4252e5fe': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ProviderCommand,apiVersion==opentestfactory.org/v1alpha1',
                    'opentestfactory.org/labelselector': 'opentestfactory.org/category==robot,opentestfactory.org/categoryPrefix==robotframework,opentestfactory.org/categoryVersion==v1',
                },
                'creationTimestamp': '2022-01-06T11:17:48.140514',
                'name': 'robotframework',
            },
            'spec': {
                'selector': {
                    'matchFields': {'apiVersion': 'opentestfactory.org/v1alpha1'},
                    'matchKind': 'ProviderCommand',
                    'matchLabels': {
                        'opentestfactory.org/category': 'robot',
                        'opentestfactory.org/categoryPrefix': 'robotframework',
                        'opentestfactory.org/categoryVersion': 'v1',
                    },
                },
                'subscriber': {'endpoint': 'http://127.0.0.1:7785/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': None,
                'publicationCount': 0,
                'publicationStatusSummary': {},
            },
        },
        '2a37283d-668d-46e7-810f-841c92aaac0f': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ProviderCommand',
                    'opentestfactory.org/labelselector': 'opentestfactory.org/category==execute,opentestfactory.org/categoryPrefix==soapui,opentestfactory.org/categoryVersion==v1',
                },
                'creationTimestamp': '2022-01-06T11:17:48.253151',
                'name': 'soapui',
            },
            'spec': {
                'selector': {
                    'matchKind': 'ProviderCommand',
                    'matchLabels': {
                        'opentestfactory.org/category': 'execute',
                        'opentestfactory.org/categoryPrefix': 'soapui',
                        'opentestfactory.org/categoryVersion': 'v1',
                    },
                },
                'subscriber': {'endpoint': 'http://127.0.0.1:7790/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': None,
                'publicationCount': 0,
                'publicationStatusSummary': {},
            },
        },
        '2bb6d41c-2e9e-43e7-a23e-7ee1da6c8a4b': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ExecutionError,apiVersion==opentestfactory.org/v1alpha1',
                    'opentestfactory.org/labelselector': '',
                },
                'creationTimestamp': '2022-01-06T11:17:47.627812',
                'name': 'arranger',
            },
            'spec': {
                'selector': {
                    'matchFields': {'apiVersion': 'opentestfactory.org/v1alpha1'},
                    'matchKind': 'ExecutionError',
                },
                'subscriber': {'endpoint': 'http://127.0.0.1:7781/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': '2022-01-06T11:53:56.444047',
                'publicationCount': 1,
                'publicationStatusSummary': {'200': 1},
            },
        },
        '306c5540-9dea-4baf-90ad-c2f9207125f1': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==Workflow,apiVersion==opentestfactory.org/v1alpha1',
                    'opentestfactory.org/labelselector': '',
                },
                'creationTimestamp': '2022-01-06T11:17:47.016920',
                'name': 'observer',
            },
            'spec': {
                'selector': {
                    'matchFields': {'apiVersion': 'opentestfactory.org/v1alpha1'},
                    'matchKind': 'Workflow',
                },
                'subscriber': {'endpoint': 'http://0.0.0.0:7775/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': '2022-01-06T11:49:52.929421',
                'publicationCount': 2,
                'publicationStatusSummary': {'200': 2},
            },
        },
        '30e492ab-e850-47f9-aef5-cd5923965ae1': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ProviderCommand',
                    'opentestfactory.org/labelselector': 'opentestfactory.org/category==create-file,opentestfactory.org/categoryPrefix==actions',
                },
                'creationTimestamp': '2022-01-06T11:17:48.344882',
                'name': 'actionprovider',
            },
            'spec': {
                'selector': {
                    'matchKind': 'ProviderCommand',
                    'matchLabels': {
                        'opentestfactory.org/category': 'create-file',
                        'opentestfactory.org/categoryPrefix': 'actions',
                    },
                },
                'subscriber': {'endpoint': 'http://127.0.0.1:7780/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': None,
                'publicationCount': 0,
                'publicationStatusSummary': {},
            },
        },
        '3363aa59-2720-4234-8230-d972fa67b801': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ReportInterpreterInput',
                    'opentestfactory.org/labelselector': '',
                },
                'creationTimestamp': '2022-01-06T11:20:39.954299',
                'name': 'surefire.interpreter',
            },
            'spec': {
                'selector': {'matchKind': 'ReportInterpreterInput'},
                'subscriber': {
                    'endpoint': 'http://127.0.0.1:10081/surefireReportInterpreter/inbox'
                },
            },
            'status': {
                'lastPublicationTimestamp': '2022-01-06T11:54:10.429826',
                'publicationCount': 2,
                'publicationStatusSummary': {'204': 2},
            },
        },
        '36d799b5-d2d2-4f76-bdca-ba7fe4f8282c': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ReportInterpreterInput',
                    'opentestfactory.org/labelselector': '',
                },
                'creationTimestamp': '2022-01-06T11:22:28.818648',
                'name': 'cypress.interpreter',
            },
            'spec': {
                'selector': {'matchKind': 'ReportInterpreterInput'},
                'subscriber': {
                    'endpoint': 'http://127.0.0.1:10082/cypressReportInterpreter/inbox'
                },
            },
            'status': {
                'lastPublicationTimestamp': '2022-01-06T11:54:10.429826',
                'publicationCount': 2,
                'publicationStatusSummary': {'204': 2},
            },
        },
        '3a4d6c1a-76ab-44e2-84fd-8f816d8684f3': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ReportInterpreterInput',
                    'opentestfactory.org/labelselector': '',
                },
                'creationTimestamp': '2022-01-06T11:20:40.176643',
                'name': 'uft.interpreter',
            },
            'spec': {
                'selector': {'matchKind': 'ReportInterpreterInput'},
                'subscriber': {
                    'endpoint': 'http://127.0.0.1:10084/uftReportInterpreter/inbox'
                },
            },
            'status': {
                'lastPublicationTimestamp': '2022-01-06T11:54:10.429826',
                'publicationCount': 2,
                'publicationStatusSummary': {'204': 2},
            },
        },
        '3a83df68-12ef-42d9-9630-3b15b1c0ee60': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ProviderCommand,apiVersion==opentestfactory.org/v1alpha1',
                    'opentestfactory.org/labelselector': 'opentestfactory.org/category==cypress,opentestfactory.org/categoryPrefix==cypress,opentestfactory.org/categoryVersion==v1',
                },
                'creationTimestamp': '2022-01-06T11:17:48.145150',
                'name': 'cypress',
            },
            'spec': {
                'selector': {
                    'matchFields': {'apiVersion': 'opentestfactory.org/v1alpha1'},
                    'matchKind': 'ProviderCommand',
                    'matchLabels': {
                        'opentestfactory.org/category': 'cypress',
                        'opentestfactory.org/categoryPrefix': 'cypress',
                        'opentestfactory.org/categoryVersion': 'v1',
                    },
                },
                'subscriber': {'endpoint': 'http://127.0.0.1:7789/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': None,
                'publicationCount': 0,
                'publicationStatusSummary': {},
            },
        },
        '3deeca97-88d2-45d7-8372-94df5ac03566': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ProviderCommand',
                    'opentestfactory.org/labelselector': 'opentestfactory.org/category==params,opentestfactory.org/categoryPrefix==junit,opentestfactory.org/categoryVersion==v1',
                },
                'creationTimestamp': '2022-01-06T11:17:48.415835',
                'name': 'junit',
            },
            'spec': {
                'selector': {
                    'matchKind': 'ProviderCommand',
                    'matchLabels': {
                        'opentestfactory.org/category': 'params',
                        'opentestfactory.org/categoryPrefix': 'junit',
                        'opentestfactory.org/categoryVersion': 'v1',
                    },
                },
                'subscriber': {'endpoint': 'http://127.0.0.1:7788/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': None,
                'publicationCount': 0,
                'publicationStatusSummary': {},
            },
        },
        '3ef8d45d-19f3-452a-8e29-87253ee99ace': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==WorkflowResult,apiVersion==opentestfactory.org/v1alpha1',
                    'opentestfactory.org/labelselector': '',
                },
                'creationTimestamp': '2022-01-06T11:17:52.811313',
                'name': 's3publisher',
            },
            'spec': {
                'selector': {
                    'matchFields': {'apiVersion': 'opentestfactory.org/v1alpha1'},
                    'matchKind': 'WorkflowResult',
                },
                'subscriber': {'endpoint': 'http://127.0.0.1:7787//inbox'},
            },
            'status': {
                'lastPublicationTimestamp': '2022-01-06T11:56:25.287014',
                'publicationCount': 4,
                'publicationStatusSummary': {'200': 4},
            },
        },
        '412efd8f-6b0a-435a-bb06-efe4face343a': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ProviderCommand',
                    'opentestfactory.org/labelselector': 'opentestfactory.org/category==params,opentestfactory.org/categoryPrefix==skf,opentestfactory.org/categoryVersion==v1',
                },
                'creationTimestamp': '2022-01-06T11:17:48.141093',
                'name': 'skf',
            },
            'spec': {
                'selector': {
                    'matchKind': 'ProviderCommand',
                    'matchLabels': {
                        'opentestfactory.org/category': 'params',
                        'opentestfactory.org/categoryPrefix': 'skf',
                        'opentestfactory.org/categoryVersion': 'v1',
                    },
                },
                'subscriber': {'endpoint': 'http://127.0.0.1:7792/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': None,
                'publicationCount': 0,
                'publicationStatusSummary': {},
            },
        },
        '42b2c995-99e3-4bb2-a1be-44ae2159078f': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ExecutionCommand',
                    'opentestfactory.org/labelselector': '',
                },
                'creationTimestamp': '2022-01-06T11:20:39.956136',
                'name': 'result.aggregator.execution-command',
            },
            'spec': {
                'selector': {'matchKind': 'ExecutionCommand'},
                'subscriber': {
                    'endpoint': 'http://127.0.0.1:10088/result-aggregator/inbox/command'
                },
            },
            'status': {
                'lastPublicationTimestamp': '2022-01-06T11:53:56.272156',
                'publicationCount': 21,
                'publicationStatusSummary': {'204': 21},
            },
        },
        '42c324d3-f792-47c4-b7b5-d2442b31c543': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ProviderCommand',
                    'opentestfactory.org/labelselector': 'opentestfactory.org/category==touch-file,opentestfactory.org/categoryPrefix==actions',
                },
                'creationTimestamp': '2022-01-06T11:17:48.575136',
                'name': 'actionprovider',
            },
            'spec': {
                'selector': {
                    'matchKind': 'ProviderCommand',
                    'matchLabels': {
                        'opentestfactory.org/category': 'touch-file',
                        'opentestfactory.org/categoryPrefix': 'actions',
                    },
                },
                'subscriber': {'endpoint': 'http://127.0.0.1:7780/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': None,
                'publicationCount': 0,
                'publicationStatusSummary': {},
            },
        },
        '43d30979-82bd-41e9-becb-8b36d3839671': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ExecutionResult,apiVersion==opentestfactory.org/v1alpha1',
                    'opentestfactory.org/labelselector': '',
                },
                'creationTimestamp': '2022-01-06T11:17:47.544189',
                'name': 'arranger',
            },
            'spec': {
                'selector': {
                    'matchFields': {'apiVersion': 'opentestfactory.org/v1alpha1'},
                    'matchKind': 'ExecutionResult',
                },
                'subscriber': {'endpoint': 'http://127.0.0.1:7781/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': '2022-01-06T11:54:01.531123',
                'publicationCount': 21,
                'publicationStatusSummary': {'200': 21},
            },
        },
        '443f16ac-8314-4ccc-94a4-29191636c124': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==AllureCollectorOutput',
                    'opentestfactory.org/labelselector': '',
                },
                'creationTimestamp': '2022-01-06T11:17:47.983777',
                'name': 'rewriter',
            },
            'spec': {
                'selector': {'matchKind': 'AllureCollectorOutput'},
                'subscriber': {'endpoint': 'http://0.0.0.0:7794//inbox'},
            },
            'status': {
                'lastPublicationTimestamp': '2022-01-06T11:56:04.789672',
                'publicationCount': 4,
                'publicationStatusSummary': {'200': 4},
            },
        },
        '4a3814c2-7fe0-409a-9a89-fc4e2cb9e72d': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ProviderCommand,apiVersion==opentestfactory.org/v1alpha1',
                    'opentestfactory.org/labelselector': 'opentestfactory.org/category==params,opentestfactory.org/categoryPrefix==robotframework,opentestfactory.org/categoryVersion==v1',
                },
                'creationTimestamp': '2022-01-06T11:17:48.344376',
                'name': 'robotframework',
            },
            'spec': {
                'selector': {
                    'matchFields': {'apiVersion': 'opentestfactory.org/v1alpha1'},
                    'matchKind': 'ProviderCommand',
                    'matchLabels': {
                        'opentestfactory.org/category': 'params',
                        'opentestfactory.org/categoryPrefix': 'robotframework',
                        'opentestfactory.org/categoryVersion': 'v1',
                    },
                },
                'subscriber': {'endpoint': 'http://127.0.0.1:7785/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': None,
                'publicationCount': 0,
                'publicationStatusSummary': {},
            },
        },
        '51a8e38d-2127-4426-9bb8-153d9a4881f7': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ProviderCommand',
                    'opentestfactory.org/labelselector': 'opentestfactory.org/category==ranorex,opentestfactory.org/categoryPrefix==ranorex,opentestfactory.org/categoryVersion==v1',
                },
                'creationTimestamp': '2022-01-06T11:19:38.701631',
                'name': 'ranorex',
            },
            'spec': {
                'selector': {
                    'matchKind': 'ProviderCommand',
                    'matchLabels': {
                        'opentestfactory.org/category': 'ranorex',
                        'opentestfactory.org/categoryPrefix': 'ranorex',
                        'opentestfactory.org/categoryVersion': 'v1',
                    },
                },
                'subscriber': {'endpoint': 'http://127.0.0.1:20003/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': None,
                'publicationCount': 0,
                'publicationStatusSummary': {},
            },
        },
        '538be6f6-9837-4adc-82c5-702171752e39': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ProviderCommand',
                    'opentestfactory.org/labelselector': 'opentestfactory.org/category==agilitest,opentestfactory.org/categoryPrefix==agilitest,opentestfactory.org/categoryVersion==v1',
                },
                'creationTimestamp': '2022-01-06T11:19:38.692014',
                'name': 'agilitest',
            },
            'spec': {
                'selector': {
                    'matchKind': 'ProviderCommand',
                    'matchLabels': {
                        'opentestfactory.org/category': 'agilitest',
                        'opentestfactory.org/categoryPrefix': 'agilitest',
                        'opentestfactory.org/categoryVersion': 'v1',
                    },
                },
                'subscriber': {'endpoint': 'http://127.0.0.1:20001/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': None,
                'publicationCount': 0,
                'publicationStatusSummary': {},
            },
        },
        '587dfa41-42d8-4181-a14f-a785121087f6': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==GeneratorResult,apiVersion==opentestfactory.org/v1alpha1',
                    'opentestfactory.org/labelselector': '',
                },
                'creationTimestamp': '2022-01-06T11:17:47.309874',
                'name': 'arranger',
            },
            'spec': {
                'selector': {
                    'matchFields': {'apiVersion': 'opentestfactory.org/v1alpha1'},
                    'matchKind': 'GeneratorResult',
                },
                'subscriber': {'endpoint': 'http://127.0.0.1:7781/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': None,
                'publicationCount': 0,
                'publicationStatusSummary': {},
            },
        },
        '5a165993-4252-4fc0-be9b-87d958fcb6b0': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ProviderCommand',
                    'opentestfactory.org/labelselector': 'opentestfactory.org/category==checkout,opentestfactory.org/categoryPrefix==action,opentestfactory.org/categoryVersion==v2',
                },
                'creationTimestamp': '2022-01-06T11:17:48.254193',
                'name': 'actionprovider',
            },
            'spec': {
                'selector': {
                    'matchKind': 'ProviderCommand',
                    'matchLabels': {
                        'opentestfactory.org/category': 'checkout',
                        'opentestfactory.org/categoryPrefix': 'action',
                        'opentestfactory.org/categoryVersion': 'v2',
                    },
                },
                'subscriber': {'endpoint': 'http://127.0.0.1:7780/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': '2022-01-06T11:50:28.004968',
                'publicationCount': 2,
                'publicationStatusSummary': {'200': 2},
            },
        },
        '5c02e93c-e925-45ba-9a73-a6f2e1c55ded': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ProviderResult,apiVersion==opentestfactory.org/v1alpha1',
                    'opentestfactory.org/labelselector': '',
                },
                'creationTimestamp': '2022-01-06T11:17:47.405869',
                'name': 'arranger',
            },
            'spec': {
                'selector': {
                    'matchFields': {'apiVersion': 'opentestfactory.org/v1alpha1'},
                    'matchKind': 'ProviderResult',
                },
                'subscriber': {'endpoint': 'http://127.0.0.1:7781/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': '2022-01-06T11:53:50.447214',
                'publicationCount': 12,
                'publicationStatusSummary': {'200': 12},
            },
        },
        '5ce7452b-17c4-4274-b16f-cca1a62ed2b3': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==WorkflowCompleted,apiVersion==opentestfactory.org/v1alpha1',
                    'opentestfactory.org/labelselector': '',
                },
                'creationTimestamp': '2022-01-06T11:17:47.075392',
                'name': 'observer',
            },
            'spec': {
                'selector': {
                    'matchFields': {'apiVersion': 'opentestfactory.org/v1alpha1'},
                    'matchKind': 'WorkflowCompleted',
                },
                'subscriber': {'endpoint': 'http://0.0.0.0:7775/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': '2022-01-06T11:37:50.353064',
                'publicationCount': 1,
                'publicationStatusSummary': {'200': 1},
            },
        },
        '5e1b2caf-6880-497b-a56d-811dfc244091': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ProviderCommand,apiVersion==opentestfactory.org/v1alpha1',
                    'opentestfactory.org/labelselector': 'opentestfactory.org/category==execute,opentestfactory.org/categoryPrefix==robotframework,opentestfactory.org/categoryVersion==v1',
                },
                'creationTimestamp': '2022-01-06T11:17:48.251612',
                'name': 'robotframework',
            },
            'spec': {
                'selector': {
                    'matchFields': {'apiVersion': 'opentestfactory.org/v1alpha1'},
                    'matchKind': 'ProviderCommand',
                    'matchLabels': {
                        'opentestfactory.org/category': 'execute',
                        'opentestfactory.org/categoryPrefix': 'robotframework',
                        'opentestfactory.org/categoryVersion': 'v1',
                    },
                },
                'subscriber': {'endpoint': 'http://127.0.0.1:7785/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': None,
                'publicationCount': 0,
                'publicationStatusSummary': {},
            },
        },
        '60ac99cd-22a4-4865-b8f4-e1c3f166ad20': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ProviderCommand,apiVersion==opentestfactory.org/v1alpha1',
                    'opentestfactory.org/labelselector': 'opentestfactory.org/category==cucumber,opentestfactory.org/categoryPrefix==cucumber,opentestfactory.org/categoryVersion==v1',
                },
                'creationTimestamp': '2022-01-06T11:17:48.568702',
                'name': 'cucumber',
            },
            'spec': {
                'selector': {
                    'matchFields': {'apiVersion': 'opentestfactory.org/v1alpha1'},
                    'matchKind': 'ProviderCommand',
                    'matchLabels': {
                        'opentestfactory.org/category': 'cucumber',
                        'opentestfactory.org/categoryPrefix': 'cucumber',
                        'opentestfactory.org/categoryVersion': 'v1',
                    },
                },
                'subscriber': {'endpoint': 'http://127.0.0.1:7784/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': None,
                'publicationCount': 0,
                'publicationStatusSummary': {},
            },
        },
        '6266b6ad-5da5-49da-bd03-ef3eeff12033': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ProviderCommand',
                    'opentestfactory.org/labelselector': 'opentestfactory.org/category==skf,opentestfactory.org/categoryPrefix==skf,opentestfactory.org/categoryVersion==v1',
                },
                'creationTimestamp': '2022-01-06T11:17:47.786227',
                'name': 'skf',
            },
            'spec': {
                'selector': {
                    'matchKind': 'ProviderCommand',
                    'matchLabels': {
                        'opentestfactory.org/category': 'skf',
                        'opentestfactory.org/categoryPrefix': 'skf',
                        'opentestfactory.org/categoryVersion': 'v1',
                    },
                },
                'subscriber': {'endpoint': 'http://127.0.0.1:7792/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': None,
                'publicationCount': 0,
                'publicationStatusSummary': {},
            },
        },
        '6456690d-88c3-44ce-82ca-cd8b5543a474': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ExecutionError,apiVersion==opentestfactory.org/v1alpha1',
                    'opentestfactory.org/labelselector': '',
                },
                'creationTimestamp': '2022-01-06T11:17:47.984094',
                'name': 'observer',
            },
            'spec': {
                'selector': {
                    'matchFields': {'apiVersion': 'opentestfactory.org/v1alpha1'},
                    'matchKind': 'ExecutionError',
                },
                'subscriber': {'endpoint': 'http://0.0.0.0:7775/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': '2022-01-06T11:53:56.444047',
                'publicationCount': 1,
                'publicationStatusSummary': {'200': 1},
            },
        },
        '68631a5e-f623-4513-bfe5-cdabedbc7ac6': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==WorkflowCompleted',
                    'opentestfactory.org/labelselector': '',
                },
                'creationTimestamp': '2022-01-06T11:20:46.334260',
                'name': 'result.aggregator.workflow-completed',
            },
            'spec': {
                'selector': {'matchKind': 'WorkflowCompleted'},
                'subscriber': {
                    'endpoint': 'http://127.0.0.1:10088/result-aggregator/inbox/workflow-completed'
                },
            },
            'status': {
                'lastPublicationTimestamp': '2022-01-06T11:37:50.353064',
                'publicationCount': 1,
                'publicationStatusSummary': {'204': 1},
            },
        },
        '691f8706-68f5-4e0a-996e-8d483256bd3d': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ReportInterpreterInput',
                    'opentestfactory.org/labelselector': '',
                },
                'creationTimestamp': '2022-01-06T11:20:40.628797',
                'name': 'soapui.interpreter',
            },
            'spec': {
                'selector': {'matchKind': 'ReportInterpreterInput'},
                'subscriber': {
                    'endpoint': 'http://127.0.0.1:10086/soapuiReportInterpreter/inbox'
                },
            },
            'status': {
                'lastPublicationTimestamp': '2022-01-06T11:54:10.429826',
                'publicationCount': 2,
                'publicationStatusSummary': {'204': 2},
            },
        },
        '6cc4ca24-9470-407e-bd52-21aa5608b583': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ProviderCommand,apiVersion==opentestfactory.org/v1alpha1',
                    'opentestfactory.org/labelselector': '',
                },
                'creationTimestamp': '2022-01-06T11:17:47.492491',
                'name': 'observer',
            },
            'spec': {
                'selector': {
                    'matchFields': {'apiVersion': 'opentestfactory.org/v1alpha1'},
                    'matchKind': 'ProviderCommand',
                },
                'subscriber': {'endpoint': 'http://0.0.0.0:7775/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': '2022-01-06T11:53:50.249571',
                'publicationCount': 12,
                'publicationStatusSummary': {'200': 12},
            },
        },
        '6cd465cc-cdf0-4822-b2df-ad9a176b4494': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ProviderCommand',
                    'opentestfactory.org/labelselector': 'opentestfactory.org/category==put-file,opentestfactory.org/categoryPrefix==actions',
                },
                'creationTimestamp': '2022-01-06T11:17:48.536316',
                'name': 'actionprovider',
            },
            'spec': {
                'selector': {
                    'matchKind': 'ProviderCommand',
                    'matchLabels': {
                        'opentestfactory.org/category': 'put-file',
                        'opentestfactory.org/categoryPrefix': 'actions',
                    },
                },
                'subscriber': {'endpoint': 'http://127.0.0.1:7780/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': None,
                'publicationCount': 0,
                'publicationStatusSummary': {},
            },
        },
        '6e74b434-02a2-4468-a5f8-0f3d6ba4bc7e': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ReportInterpreterInput',
                    'opentestfactory.org/labelselector': '',
                },
                'creationTimestamp': '2022-01-06T11:20:39.970151',
                'name': 'agilitest.interpreter',
            },
            'spec': {
                'selector': {'matchKind': 'ReportInterpreterInput'},
                'subscriber': {
                    'endpoint': 'http://127.0.0.1:10085/agilitestReportInterpreter/inbox'
                },
            },
            'status': {
                'lastPublicationTimestamp': '2022-01-06T11:54:10.429826',
                'publicationCount': 2,
                'publicationStatusSummary': {'204': 2},
            },
        },
        '72946e4f-8048-4dd5-841b-be8db296a966': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==Notification,apiVersion==opentestfactory.org/v1alpha1',
                    'opentestfactory.org/labelselector': '',
                },
                'creationTimestamp': '2022-01-06T11:17:48.146399',
                'name': 'observer',
            },
            'spec': {
                'selector': {
                    'matchFields': {'apiVersion': 'opentestfactory.org/v1alpha1'},
                    'matchKind': 'Notification',
                },
                'subscriber': {'endpoint': 'http://0.0.0.0:7775/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': '2022-01-06T11:56:30.630175',
                'publicationCount': 20,
                'publicationStatusSummary': {'200': 20},
            },
        },
        '766a427d-a08b-4b11-ac29-2acf43a0b66b': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ProviderCommand,apiVersion==opentestfactory.org/v1alpha1',
                    'opentestfactory.org/labelselector': 'opentestfactory.org/category==params,opentestfactory.org/categoryPrefix==cypress,opentestfactory.org/categoryVersion==v1',
                },
                'creationTimestamp': '2022-01-06T11:17:48.408354',
                'name': 'cypress',
            },
            'spec': {
                'selector': {
                    'matchFields': {'apiVersion': 'opentestfactory.org/v1alpha1'},
                    'matchKind': 'ProviderCommand',
                    'matchLabels': {
                        'opentestfactory.org/category': 'params',
                        'opentestfactory.org/categoryPrefix': 'cypress',
                        'opentestfactory.org/categoryVersion': 'v1',
                    },
                },
                'subscriber': {'endpoint': 'http://127.0.0.1:7789/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': None,
                'publicationCount': 0,
                'publicationStatusSummary': {},
            },
        },
        '77fbf303-0a03-4e82-8e23-d39ea2ee4aa1': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ExecutionCommand',
                    'opentestfactory.org/labelselector': '',
                },
                'creationTimestamp': '2022-01-06T11:17:47.586519',
                'name': 'agentchannel',
            },
            'spec': {
                'selector': {'matchKind': 'ExecutionCommand'},
                'subscriber': {'endpoint': 'http://0.0.0.0:24368/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': '2022-01-06T11:53:56.272156',
                'publicationCount': 21,
                'publicationStatusSummary': {'200': 21},
            },
        },
        '7a182f1c-4f33-4e5b-96db-bbba53fdfec0': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ProviderCommand,apiVersion==opentestfactory.org/v1alpha1',
                    'opentestfactory.org/labelselector': 'opentestfactory.org/category==execute,opentestfactory.org/categoryPrefix==cypress,opentestfactory.org/categoryVersion==v1',
                },
                'creationTimestamp': '2022-01-06T11:17:48.273932',
                'name': 'cypress',
            },
            'spec': {
                'selector': {
                    'matchFields': {'apiVersion': 'opentestfactory.org/v1alpha1'},
                    'matchKind': 'ProviderCommand',
                    'matchLabels': {
                        'opentestfactory.org/category': 'execute',
                        'opentestfactory.org/categoryPrefix': 'cypress',
                        'opentestfactory.org/categoryVersion': 'v1',
                    },
                },
                'subscriber': {'endpoint': 'http://127.0.0.1:7789/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': None,
                'publicationCount': 0,
                'publicationStatusSummary': {},
            },
        },
        '7d9fa345-6db9-4fd1-b9a8-6e8827a7430a': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==WorkflowResult,apiVersion==opentestfactory.org/v1alpha1',
                    'opentestfactory.org/labelselector': '',
                },
                'creationTimestamp': '2022-01-06T11:17:47.980188',
                'name': 'localpublisher',
            },
            'spec': {
                'selector': {
                    'matchFields': {'apiVersion': 'opentestfactory.org/v1alpha1'},
                    'matchKind': 'WorkflowResult',
                },
                'subscriber': {'endpoint': 'http://127.0.0.1:7783//inbox'},
            },
            'status': {
                'lastPublicationTimestamp': '2022-01-06T11:56:25.287014',
                'publicationCount': 4,
                'publicationStatusSummary': {'200': 4},
            },
        },
        '8048f79d-b96a-4104-993e-0004fc3c5ab6': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ProviderCommand',
                    'opentestfactory.org/labelselector': 'opentestfactory.org/category==params,opentestfactory.org/categoryPrefix==agilitest,opentestfactory.org/categoryVersion==v1',
                },
                'creationTimestamp': '2022-01-06T11:19:39.383696',
                'name': 'agilitest',
            },
            'spec': {
                'selector': {
                    'matchKind': 'ProviderCommand',
                    'matchLabels': {
                        'opentestfactory.org/category': 'params',
                        'opentestfactory.org/categoryPrefix': 'agilitest',
                        'opentestfactory.org/categoryVersion': 'v1',
                    },
                },
                'subscriber': {'endpoint': 'http://127.0.0.1:20001/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': '2022-01-06T11:50:46.094890',
                'publicationCount': 2,
                'publicationStatusSummary': {'200': 2},
            },
        },
        '8443ddc4-dae4-494a-b388-9f8658f81f6b': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ProviderCommand',
                    'opentestfactory.org/labelselector': 'opentestfactory.org/category==execute,opentestfactory.org/categoryPrefix==uft,opentestfactory.org/categoryVersion==v1',
                },
                'creationTimestamp': '2022-01-06T11:19:39.351633',
                'name': 'uft',
            },
            'spec': {
                'selector': {
                    'matchKind': 'ProviderCommand',
                    'matchLabels': {
                        'opentestfactory.org/category': 'execute',
                        'opentestfactory.org/categoryPrefix': 'uft',
                        'opentestfactory.org/categoryVersion': 'v1',
                    },
                },
                'subscriber': {'endpoint': 'http://127.0.0.1:20002/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': None,
                'publicationCount': 0,
                'publicationStatusSummary': {},
            },
        },
        '8aa91b88-962a-483a-b502-952c041a603f': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ProviderCommand',
                    'opentestfactory.org/labelselector': 'opentestfactory.org/category==params,opentestfactory.org/categoryPrefix==soapui,opentestfactory.org/categoryVersion==v1',
                },
                'creationTimestamp': '2022-01-06T11:17:48.346271',
                'name': 'soapui',
            },
            'spec': {
                'selector': {
                    'matchKind': 'ProviderCommand',
                    'matchLabels': {
                        'opentestfactory.org/category': 'params',
                        'opentestfactory.org/categoryPrefix': 'soapui',
                        'opentestfactory.org/categoryVersion': 'v1',
                    },
                },
                'subscriber': {'endpoint': 'http://127.0.0.1:7790/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': None,
                'publicationCount': 0,
                'publicationStatusSummary': {},
            },
        },
        '8c686ea6-03b3-4eca-ac0a-2d659560ddec': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==AllureCollectorInput',
                    'opentestfactory.org/labelselector': '',
                },
                'creationTimestamp': '2022-01-06T11:20:39.968347',
                'name': 'allure.collector',
            },
            'spec': {
                'selector': {'matchKind': 'AllureCollectorInput'},
                'subscriber': {
                    'endpoint': 'http://127.0.0.1:10070/AllureReportCollector/inbox'
                },
            },
            'status': {
                'lastPublicationTimestamp': '2022-01-06T11:54:15.248119',
                'publicationCount': 4,
                'publicationStatusSummary': {'204': 4},
            },
        },
        '8d4b1eb0-085d-43c0-9c2b-2a7eba2ce91b': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ExecutionCommand,runs-on==dummy',
                    'opentestfactory.org/labelselector': '',
                },
                'creationTimestamp': '2022-01-06T11:17:47.537312',
                'name': 'dummyee',
            },
            'spec': {
                'selector': {
                    'matchFields': {'runs-on': 'dummy'},
                    'matchKind': 'ExecutionCommand',
                },
                'subscriber': {'endpoint': 'http://127.0.0.1:7782/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': None,
                'publicationCount': 0,
                'publicationStatusSummary': {},
            },
        },
        '92efe72f-cee7-41b2-b56d-167e961796d7': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ProviderCommand',
                    'opentestfactory.org/labelselector': 'opentestfactory.org/category==execute,opentestfactory.org/categoryPrefix==agilitest,opentestfactory.org/categoryVersion==v1',
                },
                'creationTimestamp': '2022-01-06T11:19:38.961475',
                'name': 'agilitest',
            },
            'spec': {
                'selector': {
                    'matchKind': 'ProviderCommand',
                    'matchLabels': {
                        'opentestfactory.org/category': 'execute',
                        'opentestfactory.org/categoryPrefix': 'agilitest',
                        'opentestfactory.org/categoryVersion': 'v1',
                    },
                },
                'subscriber': {'endpoint': 'http://127.0.0.1:20001/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': '2022-01-06T11:51:23.638227',
                'publicationCount': 2,
                'publicationStatusSummary': {'200': 2},
            },
        },
        '931b648b-dd86-49bc-a803-8c9e48446fe6': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ProviderCommand',
                    'opentestfactory.org/labelselector': 'opentestfactory.org/category==uft,opentestfactory.org/categoryPrefix==uft,opentestfactory.org/categoryVersion==v1',
                },
                'creationTimestamp': '2022-01-06T11:19:38.700159',
                'name': 'uft',
            },
            'spec': {
                'selector': {
                    'matchKind': 'ProviderCommand',
                    'matchLabels': {
                        'opentestfactory.org/category': 'uft',
                        'opentestfactory.org/categoryPrefix': 'uft',
                        'opentestfactory.org/categoryVersion': 'v1',
                    },
                },
                'subscriber': {'endpoint': 'http://127.0.0.1:20002/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': None,
                'publicationCount': 0,
                'publicationStatusSummary': {},
            },
        },
        '95dfeab7-b9dc-443d-adc8-ad3f2311c144': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ProviderResult,apiVersion==opentestfactory.org/v1alpha1',
                    'opentestfactory.org/labelselector': '',
                },
                'creationTimestamp': '2022-01-06T11:17:47.620868',
                'name': 'observer',
            },
            'spec': {
                'selector': {
                    'matchFields': {'apiVersion': 'opentestfactory.org/v1alpha1'},
                    'matchKind': 'ProviderResult',
                },
                'subscriber': {'endpoint': 'http://0.0.0.0:7775/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': '2022-01-06T11:53:50.447214',
                'publicationCount': 12,
                'publicationStatusSummary': {'200': 12},
            },
        },
        '98dac75f-3142-4cba-9780-d33afad0c6ab': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ExecutionResult,apiVersion==opentestfactory.org/v1alpha1',
                    'opentestfactory.org/labelselector': '',
                },
                'creationTimestamp': '2022-01-06T11:17:47.728400',
                'name': 'observer',
            },
            'spec': {
                'selector': {
                    'matchFields': {'apiVersion': 'opentestfactory.org/v1alpha1'},
                    'matchKind': 'ExecutionResult',
                },
                'subscriber': {'endpoint': 'http://0.0.0.0:7775/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': '2022-01-06T11:54:01.531123',
                'publicationCount': 21,
                'publicationStatusSummary': {'200': 21},
            },
        },
        '9b31c341-9e98-4ef4-ab86-4612eb2f44c6': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ProviderCommand',
                    'opentestfactory.org/labelselector': 'opentestfactory.org/category==execute,opentestfactory.org/categoryPrefix==ranorex,opentestfactory.org/categoryVersion==v1',
                },
                'creationTimestamp': '2022-01-06T11:19:39.305174',
                'name': 'ranorex',
            },
            'spec': {
                'selector': {
                    'matchKind': 'ProviderCommand',
                    'matchLabels': {
                        'opentestfactory.org/category': 'execute',
                        'opentestfactory.org/categoryPrefix': 'ranorex',
                        'opentestfactory.org/categoryVersion': 'v1',
                    },
                },
                'subscriber': {'endpoint': 'http://127.0.0.1:20003/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': None,
                'publicationCount': 0,
                'publicationStatusSummary': {},
            },
        },
        'a2ca56a9-908d-45ea-a7f3-fbc7670b0484': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ProviderCommand',
                    'opentestfactory.org/labelselector': 'opentestfactory.org/category==params,opentestfactory.org/categoryPrefix==ranorex,opentestfactory.org/categoryVersion==v1',
                },
                'creationTimestamp': '2022-01-06T11:19:39.426913',
                'name': 'ranorex',
            },
            'spec': {
                'selector': {
                    'matchKind': 'ProviderCommand',
                    'matchLabels': {
                        'opentestfactory.org/category': 'params',
                        'opentestfactory.org/categoryPrefix': 'ranorex',
                        'opentestfactory.org/categoryVersion': 'v1',
                    },
                },
                'subscriber': {'endpoint': 'http://127.0.0.1:20003/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': None,
                'publicationCount': 0,
                'publicationStatusSummary': {},
            },
        },
        'a56b4695-5113-4b81-8277-5d1b4a93c298': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ProviderCommand',
                    'opentestfactory.org/labelselector': 'opentestfactory.org/category==params,opentestfactory.org/categoryPrefix==postman,opentestfactory.org/categoryVersion==v1',
                },
                'creationTimestamp': '2022-01-06T11:17:48.142471',
                'name': 'postman',
            },
            'spec': {
                'selector': {
                    'matchKind': 'ProviderCommand',
                    'matchLabels': {
                        'opentestfactory.org/category': 'params',
                        'opentestfactory.org/categoryPrefix': 'postman',
                        'opentestfactory.org/categoryVersion': 'v1',
                    },
                },
                'subscriber': {'endpoint': 'http://127.0.0.1:7793/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': None,
                'publicationCount': 0,
                'publicationStatusSummary': {},
            },
        },
        'a6cdad7e-3cd9-4b48-9ce1-3af65de04b48': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ExecutionResult',
                    'opentestfactory.org/labelselector': '',
                },
                'creationTimestamp': '2022-01-06T11:17:47.480309',
                'name': 'localcleaner',
            },
            'spec': {
                'selector': {'matchKind': 'ExecutionResult'},
                'subscriber': {'endpoint': 'http://0.0.0.0:7777/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': '2022-01-06T11:54:01.531123',
                'publicationCount': 21,
                'publicationStatusSummary': {'200': 21},
            },
        },
        'a7430a95-6ab1-4e89-bcf8-c42097ca7d68': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ReportInterpreterInput',
                    'opentestfactory.org/labelselector': '',
                },
                'creationTimestamp': '2022-01-06T11:19:17.647407',
                'name': 'uft.interpreter',
            },
            'spec': {
                'selector': {'matchKind': 'ReportInterpreterInput'},
                'subscriber': {
                    'endpoint': 'http://127.0.0.1:10084/uftReportInterpreter/inbox'
                },
            },
            'status': {
                'lastPublicationTimestamp': '2022-01-06T11:54:10.429826',
                'publicationCount': 2,
                'publicationStatusSummary': {'204': 2},
            },
        },
        'a853c1e4-5275-48ef-a637-a695c2c767d8': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==GeneratorCommand',
                    'opentestfactory.org/labelselector': 'opentestfactory.org/categoryPrefix==tm.squashtest.org,opentestfactory.org/category==tm.generator,opentestfactory.org/categoryVersion==v1',
                },
                'creationTimestamp': '2022-01-06T11:20:39.951978',
                'name': 'tm.generator.premium',
            },
            'spec': {
                'selector': {
                    'matchKind': 'GeneratorCommand',
                    'matchLabels': {
                        'opentestfactory.org/category': 'tm.generator',
                        'opentestfactory.org/categoryPrefix': 'tm.squashtest.org',
                        'opentestfactory.org/categoryVersion': 'v1',
                    },
                },
                'subscriber': {'endpoint': 'http://127.0.0.1:10065/generator/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': None,
                'publicationCount': 0,
                'publicationStatusSummary': {},
            },
        },
        'abb150d5-999b-4aa9-a122-188c7428e3c2': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ExecutionResult',
                    'opentestfactory.org/labelselector': '',
                },
                'creationTimestamp': '2022-01-06T11:20:46.280512',
                'name': 'result.aggregator.execution-result',
            },
            'spec': {
                'selector': {'matchKind': 'ExecutionResult'},
                'subscriber': {
                    'endpoint': 'http://127.0.0.1:10088/result-aggregator/inbox/result'
                },
            },
            'status': {
                'lastPublicationTimestamp': '2022-01-06T11:54:01.531123',
                'publicationCount': 21,
                'publicationStatusSummary': {'204': 21},
            },
        },
        'ad4cee83-717e-4a55-9a27-23ec31079f8b': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==WorkflowCanceled,apiVersion==opentestfactory.org/v1alpha1',
                    'opentestfactory.org/labelselector': '',
                },
                'creationTimestamp': '2022-01-06T11:17:47.727827',
                'name': 'arranger',
            },
            'spec': {
                'selector': {
                    'matchFields': {'apiVersion': 'opentestfactory.org/v1alpha1'},
                    'matchKind': 'WorkflowCanceled',
                },
                'subscriber': {'endpoint': 'http://127.0.0.1:7781/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': '2022-01-06T11:53:56.553301',
                'publicationCount': 1,
                'publicationStatusSummary': {'200': 1},
            },
        },
        'ade7052c-5f4d-4656-b528-0dd4c88515e9': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ProviderCommand',
                    'opentestfactory.org/labelselector': 'opentestfactory.org/category==execute,opentestfactory.org/categoryPrefix==postman,opentestfactory.org/categoryVersion==v1',
                },
                'creationTimestamp': '2022-01-06T11:17:47.978364',
                'name': 'postman',
            },
            'spec': {
                'selector': {
                    'matchKind': 'ProviderCommand',
                    'matchLabels': {
                        'opentestfactory.org/category': 'execute',
                        'opentestfactory.org/categoryPrefix': 'postman',
                        'opentestfactory.org/categoryVersion': 'v1',
                    },
                },
                'subscriber': {'endpoint': 'http://127.0.0.1:7793/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': None,
                'publicationCount': 0,
                'publicationStatusSummary': {},
            },
        },
        'ae78830f-4522-4a7b-adb4-6db863551132': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ExecutionResult,apiVersion==opentestfactory.org/v1alpha1',
                    'opentestfactory.org/labelselector': '',
                },
                'creationTimestamp': '2022-01-06T11:17:52.786126',
                'name': 's3publisher',
            },
            'spec': {
                'selector': {
                    'matchFields': {'apiVersion': 'opentestfactory.org/v1alpha1'},
                    'matchKind': 'ExecutionResult',
                },
                'subscriber': {'endpoint': 'http://127.0.0.1:7787//inbox'},
            },
            'status': {
                'lastPublicationTimestamp': '2022-01-06T11:54:01.531123',
                'publicationCount': 21,
                'publicationStatusSummary': {'200': 21},
            },
        },
        'af28b0eb-f160-404e-8114-5925f1d10df9': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==GeneratorCommand,apiVersion==opentestfactory.org/v1alpha1',
                    'opentestfactory.org/labelselector': '',
                },
                'creationTimestamp': '2022-01-06T11:17:47.276657',
                'name': 'observer',
            },
            'spec': {
                'selector': {
                    'matchFields': {'apiVersion': 'opentestfactory.org/v1alpha1'},
                    'matchKind': 'GeneratorCommand',
                },
                'subscriber': {'endpoint': 'http://0.0.0.0:7775/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': None,
                'publicationCount': 0,
                'publicationStatusSummary': {},
            },
        },
        'b34a8e76-6b79-4f42-bac7-057be29b4262': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ProviderCommand,apiVersion==opentestfactory.org/v1alpha1',
                    'opentestfactory.org/labelselector': 'opentestfactory.org/category==execute,opentestfactory.org/categoryPrefix==cucumber5,opentestfactory.org/categoryVersion==v1',
                },
                'creationTimestamp': '2022-01-06T11:17:48.150081',
                'name': 'cucumber',
            },
            'spec': {
                'selector': {
                    'matchFields': {'apiVersion': 'opentestfactory.org/v1alpha1'},
                    'matchKind': 'ProviderCommand',
                    'matchLabels': {
                        'opentestfactory.org/category': 'execute',
                        'opentestfactory.org/categoryPrefix': 'cucumber5',
                        'opentestfactory.org/categoryVersion': 'v1',
                    },
                },
                'subscriber': {'endpoint': 'http://127.0.0.1:7784/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': None,
                'publicationCount': 0,
                'publicationStatusSummary': {},
            },
        },
        'bb1a07a0-d708-4719-b1ea-4e5704c62ad3': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==WorkflowCanceled',
                    'opentestfactory.org/labelselector': '',
                },
                'creationTimestamp': '2022-01-06T11:17:47.367653',
                'name': 'localcleaner',
            },
            'spec': {
                'selector': {'matchKind': 'WorkflowCanceled'},
                'subscriber': {'endpoint': 'http://0.0.0.0:7777/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': '2022-01-06T11:53:56.553301',
                'publicationCount': 1,
                'publicationStatusSummary': {'200': 1},
            },
        },
        'be1bb5b9-6ffc-4710-84c4-597a9b2109f5': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ProviderCommand',
                    'opentestfactory.org/labelselector': 'opentestfactory.org/category==soapui,opentestfactory.org/categoryPrefix==soapui,opentestfactory.org/categoryVersion==v1',
                },
                'creationTimestamp': '2022-01-06T11:17:47.987493',
                'name': 'soapui',
            },
            'spec': {
                'selector': {
                    'matchKind': 'ProviderCommand',
                    'matchLabels': {
                        'opentestfactory.org/category': 'soapui',
                        'opentestfactory.org/categoryPrefix': 'soapui',
                        'opentestfactory.org/categoryVersion': 'v1',
                    },
                },
                'subscriber': {'endpoint': 'http://127.0.0.1:7790/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': None,
                'publicationCount': 0,
                'publicationStatusSummary': {},
            },
        },
        'c19dd6a0-1984-4992-bbaf-b67dcf9cd65c': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ReportInterpreterInput',
                    'opentestfactory.org/labelselector': '',
                },
                'creationTimestamp': '2022-01-06T11:20:39.819308',
                'name': 'robot.interpreter',
            },
            'spec': {
                'selector': {'matchKind': 'ReportInterpreterInput'},
                'subscriber': {
                    'endpoint': 'http://127.0.0.1:10080/robotReportInterpreter/inbox'
                },
            },
            'status': {
                'lastPublicationTimestamp': '2022-01-06T11:54:10.429826',
                'publicationCount': 2,
                'publicationStatusSummary': {'204': 2},
            },
        },
        'c1a3f9ec-5d2f-4517-9ed2-c6686a9307b9': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ProviderCommand',
                    'opentestfactory.org/labelselector': 'opentestfactory.org/category==get-files,opentestfactory.org/categoryPrefix==actions',
                },
                'creationTimestamp': '2022-01-06T11:17:48.494511',
                'name': 'actionprovider',
            },
            'spec': {
                'selector': {
                    'matchKind': 'ProviderCommand',
                    'matchLabels': {
                        'opentestfactory.org/category': 'get-files',
                        'opentestfactory.org/categoryPrefix': 'actions',
                    },
                },
                'subscriber': {'endpoint': 'http://127.0.0.1:7780/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': None,
                'publicationCount': 0,
                'publicationStatusSummary': {},
            },
        },
        'c251b70d-913c-4a91-aaa4-ae34b7388d27': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ProviderCommand,apiVersion==opentestfactory.org/v1alpha1',
                    'opentestfactory.org/labelselector': 'opentestfactory.org/category==execute,opentestfactory.org/categoryPrefix==cucumber,opentestfactory.org/categoryVersion==v1',
                },
                'creationTimestamp': '2022-01-06T11:17:48.343155',
                'name': 'cucumber',
            },
            'spec': {
                'selector': {
                    'matchFields': {'apiVersion': 'opentestfactory.org/v1alpha1'},
                    'matchKind': 'ProviderCommand',
                    'matchLabels': {
                        'opentestfactory.org/category': 'execute',
                        'opentestfactory.org/categoryPrefix': 'cucumber',
                        'opentestfactory.org/categoryVersion': 'v1',
                    },
                },
                'subscriber': {'endpoint': 'http://127.0.0.1:7784/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': None,
                'publicationCount': 0,
                'publicationStatusSummary': {},
            },
        },
        'c9e11fda-5343-47b2-a3df-b03eac2de93b': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==WorkflowCompleted',
                    'opentestfactory.org/labelselector': '',
                },
                'creationTimestamp': '2022-01-06T11:17:47.225384',
                'name': 'localcleaner',
            },
            'spec': {
                'selector': {'matchKind': 'WorkflowCompleted'},
                'subscriber': {'endpoint': 'http://0.0.0.0:7777/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': '2022-01-06T11:37:50.353064',
                'publicationCount': 1,
                'publicationStatusSummary': {'200': 1},
            },
        },
        'cb80f08e-eba4-4fac-94d2-fd72d3e860a5': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ProviderCommand',
                    'opentestfactory.org/labelselector': 'opentestfactory.org/category==mvntest,opentestfactory.org/categoryPrefix==junit,opentestfactory.org/categoryVersion==v1',
                },
                'creationTimestamp': '2022-01-06T11:17:48.147385',
                'name': 'junit',
            },
            'spec': {
                'selector': {
                    'matchKind': 'ProviderCommand',
                    'matchLabels': {
                        'opentestfactory.org/category': 'mvntest',
                        'opentestfactory.org/categoryPrefix': 'junit',
                        'opentestfactory.org/categoryVersion': 'v1',
                    },
                },
                'subscriber': {'endpoint': 'http://127.0.0.1:7788/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': None,
                'publicationCount': 0,
                'publicationStatusSummary': {},
            },
        },
        'cc623819-e8a2-4768-9d13-7f810bd1f34a': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==SuiteDefinitionReceipt',
                    'opentestfactory.org/labelselector': '',
                },
                'creationTimestamp': '2022-01-06T11:20:46.048925',
                'name': 'tm.generator.premium',
            },
            'spec': {
                'selector': {'matchKind': 'SuiteDefinitionReceipt'},
                'subscriber': {
                    'endpoint': 'http://127.0.0.1:10065/generator/inbox/suiteReceipt'
                },
            },
            'status': {
                'lastPublicationTimestamp': '2022-01-06T11:49:55.574272',
                'publicationCount': 2,
                'publicationStatusSummary': {'204': 2},
            },
        },
        'cf34ab06-7962-451e-9c42-724b7a041a02': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ExecutionCommand,apiVersion==opentestfactory.org/v1alpha1',
                    'opentestfactory.org/labelselector': '',
                },
                'creationTimestamp': '2022-01-06T11:17:47.658454',
                'name': 'observer',
            },
            'spec': {
                'selector': {
                    'matchFields': {'apiVersion': 'opentestfactory.org/v1alpha1'},
                    'matchKind': 'ExecutionCommand',
                },
                'subscriber': {'endpoint': 'http://0.0.0.0:7775/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': '2022-01-06T11:53:56.272156',
                'publicationCount': 21,
                'publicationStatusSummary': {'200': 21},
            },
        },
        'd00457e8-c898-4e8f-9d48-8d7ba3d6dd28': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ExecutionResult,apiVersion==opentestfactory.org/v1alpha1',
                    'opentestfactory.org/labelselector': '',
                },
                'creationTimestamp': '2022-01-06T11:17:47.787873',
                'name': 'localpublisher',
            },
            'spec': {
                'selector': {
                    'matchFields': {'apiVersion': 'opentestfactory.org/v1alpha1'},
                    'matchKind': 'ExecutionResult',
                },
                'subscriber': {'endpoint': 'http://127.0.0.1:7783//inbox'},
            },
            'status': {
                'lastPublicationTimestamp': '2022-01-06T11:54:01.531123',
                'publicationCount': 21,
                'publicationStatusSummary': {'200': 21},
            },
        },
        'd1ad5c81-0480-45f7-b899-8b1d82f37395': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ProviderCommand',
                    'opentestfactory.org/labelselector': 'opentestfactory.org/category==postman,opentestfactory.org/categoryPrefix==postman,opentestfactory.org/categoryVersion==v1',
                },
                'creationTimestamp': '2022-01-06T11:17:47.736973',
                'name': 'postman',
            },
            'spec': {
                'selector': {
                    'matchKind': 'ProviderCommand',
                    'matchLabels': {
                        'opentestfactory.org/category': 'postman',
                        'opentestfactory.org/categoryPrefix': 'postman',
                        'opentestfactory.org/categoryVersion': 'v1',
                    },
                },
                'subscriber': {'endpoint': 'http://127.0.0.1:7793/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': None,
                'publicationCount': 0,
                'publicationStatusSummary': {},
            },
        },
        'd557742f-a7d1-4c31-875a-ce480b181086': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==WorkflowCanceled,apiVersion==opentestfactory.org/v1alpha1',
                    'opentestfactory.org/labelselector': '',
                },
                'creationTimestamp': '2022-01-06T11:17:47.189275',
                'name': 'observer',
            },
            'spec': {
                'selector': {
                    'matchFields': {'apiVersion': 'opentestfactory.org/v1alpha1'},
                    'matchKind': 'WorkflowCanceled',
                },
                'subscriber': {'endpoint': 'http://0.0.0.0:7775/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': '2022-01-06T11:53:56.553301',
                'publicationCount': 1,
                'publicationStatusSummary': {'200': 1},
            },
        },
        'dc5cbc64-0675-4e5f-9949-07b29a74fbfd': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ProviderCommand,apiVersion==opentestfactory.org/v1alpha1',
                    'opentestfactory.org/labelselector': 'opentestfactory.org/category==params,opentestfactory.org/categoryPrefix==cucumber,opentestfactory.org/categoryVersion==v1',
                },
                'creationTimestamp': '2022-01-06T11:17:48.470592',
                'name': 'cucumber',
            },
            'spec': {
                'selector': {
                    'matchFields': {'apiVersion': 'opentestfactory.org/v1alpha1'},
                    'matchKind': 'ProviderCommand',
                    'matchLabels': {
                        'opentestfactory.org/category': 'params',
                        'opentestfactory.org/categoryPrefix': 'cucumber',
                        'opentestfactory.org/categoryVersion': 'v1',
                    },
                },
                'subscriber': {'endpoint': 'http://127.0.0.1:7784/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': None,
                'publicationCount': 0,
                'publicationStatusSummary': {},
            },
        },
        'e3cc0af6-93fb-49d1-931b-2e77a337faf4': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ReportInterpreterOutput',
                    'opentestfactory.org/labelselector': '',
                },
                'creationTimestamp': '2022-01-06T11:20:47.523726',
                'name': 'tm.publisher',
            },
            'spec': {
                'selector': {'matchKind': 'ReportInterpreterOutput'},
                'subscriber': {
                    'endpoint': 'http://127.0.0.1:10066/publisher/inbox/reportInterpreterOutput'
                },
            },
            'status': {
                'lastPublicationTimestamp': '2022-01-06T11:55:27.752523',
                'publicationCount': 2,
                'publicationStatusSummary': {'204': 2},
            },
        },
        'e5fc5414-22e3-4d84-bdc2-a67852bb8a69': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ExecutionCommand',
                    'opentestfactory.org/labelselector': '',
                },
                'creationTimestamp': '2022-01-06T11:17:52.453118',
                'name': 'sshchannel',
            },
            'spec': {
                'selector': {'matchKind': 'ExecutionCommand'},
                'subscriber': {'endpoint': 'http://127.0.0.1:9365/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': '2022-01-06T11:53:56.272156',
                'publicationCount': 21,
                'publicationStatusSummary': {'200': 21},
            },
        },
        'ed4c6f95-a312-440d-908c-3784f115cdf2': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ExecutionCommand',
                    'opentestfactory.org/labelselector': '',
                },
                'creationTimestamp': '2022-01-06T11:17:48.093140',
                'name': 'inceptionee',
            },
            'spec': {
                'selector': {'matchKind': 'ExecutionCommand'},
                'subscriber': {'endpoint': 'http://127.0.0.1:7791/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': '2022-01-06T11:53:56.272156',
                'publicationCount': 21,
                'publicationStatusSummary': {'200': 21},
            },
        },
        'ef576fee-11c4-4e7b-869b-b3ecb7f605e7': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ProviderCommand',
                    'opentestfactory.org/labelselector': 'opentestfactory.org/category==get-file,opentestfactory.org/categoryPrefix==actions',
                },
                'creationTimestamp': '2022-01-06T11:17:48.462568',
                'name': 'actionprovider',
            },
            'spec': {
                'selector': {
                    'matchKind': 'ProviderCommand',
                    'matchLabels': {
                        'opentestfactory.org/category': 'get-file',
                        'opentestfactory.org/categoryPrefix': 'actions',
                    },
                },
                'subscriber': {'endpoint': 'http://127.0.0.1:7780/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': '2022-01-06T11:53:50.249571',
                'publicationCount': 4,
                'publicationStatusSummary': {'200': 4},
            },
        },
        'f3bcfc95-e1dd-4506-813f-309be8bb3dcb': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ExpectedSuiteDefinition',
                    'opentestfactory.org/labelselector': '',
                },
                'creationTimestamp': '2022-01-06T11:20:39.957919',
                'name': 'tm.publisher',
            },
            'spec': {
                'selector': {'matchKind': 'ExpectedSuiteDefinition'},
                'subscriber': {
                    'endpoint': 'http://127.0.0.1:10066/publisher/inbox/suiteDefinition'
                },
            },
            'status': {
                'lastPublicationTimestamp': '2022-01-06T11:49:54.703796',
                'publicationCount': 2,
                'publicationStatusSummary': {'204': 2},
            },
        },
        'f59e0d81-5731-4490-8e26-ed9baa8e24b9': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ReportInterpreterInput',
                    'opentestfactory.org/labelselector': '',
                },
                'creationTimestamp': '2022-01-06T11:18:59.992674',
                'name': 'ranorex.interpreter',
            },
            'spec': {
                'selector': {'matchKind': 'ReportInterpreterInput'},
                'subscriber': {
                    'endpoint': 'http://127.0.0.1:10087/ranorexReportInterpreter/inbox'
                },
            },
            'status': {
                'lastPublicationTimestamp': '2022-01-06T11:54:10.429826',
                'publicationCount': 2,
                'publicationStatusSummary': {'204': 2},
            },
        },
        'f7ea14b6-6eba-49e6-a812-47f8c62bdbda': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ReportInterpreterInput',
                    'opentestfactory.org/labelselector': '',
                },
                'creationTimestamp': '2022-01-06T11:20:40.465628',
                'name': 'cucumber.interpreter',
            },
            'spec': {
                'selector': {'matchKind': 'ReportInterpreterInput'},
                'subscriber': {
                    'endpoint': 'http://127.0.0.1:10083/cucumberReportInterpreter/inbox'
                },
            },
            'status': {
                'lastPublicationTimestamp': '2022-01-06T11:54:10.429826',
                'publicationCount': 2,
                'publicationStatusSummary': {'204': 2},
            },
        },
        'f91ea647-58d7-4824-9fd1-d1da8dcc260d': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==AllureCollectorOutput',
                    'opentestfactory.org/labelselector': '',
                },
                'creationTimestamp': '2022-01-06T11:20:47.543662',
                'name': 'tm.publisher',
            },
            'spec': {
                'selector': {'matchKind': 'AllureCollectorOutput'},
                'subscriber': {
                    'endpoint': 'http://127.0.0.1:10066/publisher/inbox/AllureCollectorOutput'
                },
            },
            'status': {
                'lastPublicationTimestamp': '2022-01-06T11:56:04.806137',
                'publicationCount': 4,
                'publicationStatusSummary': {'204': 4},
            },
        },
        'fbe4fe8c-76a1-41d2-ab25-fb73c8750ad0': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ProviderCommand',
                    'opentestfactory.org/labelselector': 'opentestfactory.org/category==execute,opentestfactory.org/categoryPrefix==skf,opentestfactory.org/categoryVersion==v1',
                },
                'creationTimestamp': '2022-01-06T11:17:47.896269',
                'name': 'skf',
            },
            'spec': {
                'selector': {
                    'matchKind': 'ProviderCommand',
                    'matchLabels': {
                        'opentestfactory.org/category': 'execute',
                        'opentestfactory.org/categoryPrefix': 'skf',
                        'opentestfactory.org/categoryVersion': 'v1',
                    },
                },
                'subscriber': {'endpoint': 'http://127.0.0.1:7792/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': None,
                'publicationCount': 0,
                'publicationStatusSummary': {},
            },
        },
        'fe16009c-9f7b-4d86-8a33-da08ac6473c8': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==Workflow,apiVersion==opentestfactory.org/v1alpha1',
                    'opentestfactory.org/labelselector': '',
                },
                'creationTimestamp': '2022-01-06T11:17:47.200199',
                'name': 'arranger',
            },
            'spec': {
                'selector': {
                    'matchFields': {'apiVersion': 'opentestfactory.org/v1alpha1'},
                    'matchKind': 'Workflow',
                },
                'subscriber': {'endpoint': 'http://127.0.0.1:7781/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': '2022-01-06T11:49:52.929421',
                'publicationCount': 2,
                'publicationStatusSummary': {'200': 2},
            },
        },
        'ffe4515e-840a-4918-a915-df49e55b4873': {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Subscription',
            'metadata': {
                'annotations': {
                    'opentestfactory.org/fieldselector': 'kind==ProviderCommand',
                    'opentestfactory.org/labelselector': 'opentestfactory.org/category==execute,opentestfactory.org/categoryPrefix==junit,opentestfactory.org/categoryVersion==v1',
                },
                'creationTimestamp': '2022-01-06T11:17:48.269272',
                'name': 'junit',
            },
            'spec': {
                'selector': {
                    'matchKind': 'ProviderCommand',
                    'matchLabels': {
                        'opentestfactory.org/category': 'execute',
                        'opentestfactory.org/categoryPrefix': 'junit',
                        'opentestfactory.org/categoryVersion': 'v1',
                    },
                },
                'subscriber': {'endpoint': 'http://127.0.0.1:7788/inbox'},
            },
            'status': {
                'lastPublicationTimestamp': None,
                'publicationCount': 0,
                'publicationStatusSummary': {},
            },
        },
    },
    'kind': 'SubscriptionsList',
}

########################################################################
# Helpers


########################################################################


class TestReady(unittest.TestCase):
    """Unit tests for ready."""

    # main

    def test_main_1(self):
        args = ['', '--yada=bar', '--yada=baz']
        with patch.object(sys, 'argv', args):
            self.assertRaises(SystemExit, opentf.tools.ready.main)

    def test_main_2(self):
        args = ['', '--services', 'bar', '--host', 'hOsT']
        with patch.object(sys, 'argv', args):
            self.assertRaises(SystemExit, opentf.tools.ready.main)

    def test_main_3(self):
        args = [
            '',
            '--services',
            'bar',
            '--container',
            'cOnTaInEr',
            '--host',
            'hOsT',
            '--token',
            'tOkEn',
        ]
        with patch.object(sys, 'argv', args):
            self.assertRaises(SystemExit, opentf.tools.ready.main)

    def test_main_4(self):
        args = [
            '',
            '--services',
            'bar',
            '--host',
            'hOsT',
            '--token',
            'tOkEn',
        ]
        mock = MagicMock(return_value=None)
        with patch.object(sys, 'argv', args), patch(
            'opentf.tools.ready.wait_till_ready', mock
        ):
            self.assertRaises(SystemExit, opentf.tools.ready.main)

    # is_container_ready

    def test_is_container_ready_1(self):
        mock = MagicMock()
        mock.return_value.status = 'running'
        with patch('opentf.tools.ready.get_container', mock):
            result = opentf.tools.ready.is_container_ready('fOO')
        self.assertTrue(result)

    def test_is_container_ready_2(self):
        mock = MagicMock()
        with patch('opentf.tools.ready.get_container', mock):
            result = opentf.tools.ready.is_container_ready('fOO')
        self.assertFalse(result)

    # validate_checksum

    def test_validate_checksum_1(self):
        result = opentf.tools.ready.validate_checksum(['a', 'b', 'c'], '')
        self.assertFalse(result)

    def test_validate_checksum_2(self):
        result = opentf.tools.ready.validate_checksum(
            ['a', 'b', 'c'],
            '35d95694d3f160215db293c7899daa5907837838fb4b8119ed713e32446c1266',
        )
        self.assertTrue(result)

    # _find

    def test_find_1(self):
        result = list(_find('foo', {}))
        self.assertFalse(result)

    def test_find_2(self):
        result = list(_find('foo', {'foo': 'bar', 'baz': 'foobar'}))
        self.assertEqual(len(result), 1)
        self.assertEqual(result[0], 'bar')

    def test_find_3(self):
        result = list(
            _find(
                'foo',
                {'yada': [{'foo': 'bar'}, {'foobar': 'baz'}, {'a': {'foo': 'bar2'}}]},
            )
        )
        self.assertEqual(len(result), 2)
        self.assertTrue('bar' in result)
        self.assertTrue('bar2' in result)

    # get_checked_json_file

    def test_get_checked_json_file_1(self):
        mock = MagicMock()
        mock.return_value = INIT_SERVICES_OK
        with patch('opentf.tools.ready.get_init_services_json', mock):
            result = opentf.tools.ready.get_checked_json_file('')
        self.assertTrue(result)

    def test_get_checked_json_file_2(self):
        mock = MagicMock()
        mock.return_value = INIT_SERVICES_BAD
        with patch('opentf.tools.ready.get_init_services_json', mock):
            result = opentf.tools.ready.get_checked_json_file('')
        self.assertTrue(result is None)

    # get_checked_subscribers_list

    def test_get_checked_subscribers_list_1(self):
        mock = MagicMock()
        mock.return_value.json = lambda: SUBSCRIPTIONS_OK
        with patch('opentf.tools.ready.requests.get', mock):
            result = opentf.tools.ready.get_checked_subscribers_list()
        print(result)

    # are_ready_subscribers

    def test_are_ready_subscribers_1(self):
        mock = MagicMock()
        mock.return_value = SUBSCRIBERS_OK
        with patch('opentf.tools.ready.get_checked_subscribers_list', mock):
            result = opentf.tools.ready.are_ready_subscribers(
                INIT_SERVICES_OK['services']
            )
        self.assertTrue(result)

    # wait_till_ready

    def test_wait_till_ready(self):
        mock = MagicMock(return_value=True)
        with patch('opentf.tools.ready.SERVICES', []), patch(
            'opentf.tools.ready.CONTAINER_ID', None
        ), patch('opentf.tools.ready.are_ready_subscribers', mock):
            opentf.tools.ready.wait_till_ready()
        mock.assert_called_once_with([])
