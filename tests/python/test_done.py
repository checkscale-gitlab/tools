# Copyright (c) 2022 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""tools unit tests."""

import sys
import unittest

from unittest.mock import MagicMock, patch

from requests import Response, exceptions

import yaml

import opentf.tools.done


########################################################################
# Datasets


########################################################################
# Helpers


########################################################################


class TestDone(unittest.TestCase):
    """Unit tests for done."""

    # wait_until_idle

    def test_wait_until_idle_1(self):
        mockresponse = Response()
        mockresponse.status_code = 200
        mockresponse.json = lambda: {'details': {'status': 'IDLE'}}
        mock = MagicMock(return_value=mockresponse)
        with patch('requests.get', mock):
            result = opentf.tools.done.wait_until_idle('', {}, 1, 2)
        self.assertTrue(result)

    def test_wait_until_idle_2(self):
        mockresponse = Response()
        mockresponse.status_code = 200
        mockresponse.json = lambda: {'details': {'status': 'BUSY'}}
        mock = MagicMock(return_value=mockresponse)
        with patch('requests.get', mock):
            result = opentf.tools.done.wait_until_idle('', {}, 1, 2)
        self.assertFalse(result)

    # main

    def test_main_1(self):
        args = ['', '--yada=bar', '--yada=baz']
        with patch.object(sys, 'argv', args):
            self.assertRaises(SystemExit, opentf.tools.done.main)

    def test_main_2(self):
        args = ['', '--services', 'bar', '--host', 'hOsT']
        with patch.object(sys, 'argv', args):
            self.assertRaises(SystemExit, opentf.tools.done.main)

    def test_main_3(self):
        args = ['', '--host', 'hOsT', '--token', 'tOkEn']
        mock = MagicMock(return_value=True)
        mock2 = MagicMock()
        with patch.object(sys, 'argv', args), patch(
            'opentf.tools.done.wait_until_idle', mock
        ), patch('sys.exit', mock2):
            opentf.tools.done.main()
        mock.assert_called_once()
        mock2.assert_called_once_with(0)

    def test_main_4(self):
        args = ['', '--host', 'hOsT', '--token', 'tOkEn']
        mock = MagicMock(return_value=False)
        mock2 = MagicMock()
        with patch.object(sys, 'argv', args), patch(
            'opentf.tools.done.wait_until_idle', mock
        ), patch('sys.exit', mock2):
            opentf.tools.done.main()
        mock.assert_called_once()
        mock2.assert_called_once_with(1)
