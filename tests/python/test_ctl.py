# Copyright (c) 2022 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""tools unit tests."""

import sys
import unittest

from unittest.mock import MagicMock, patch, mock_open

import jwt

from requests import Response, exceptions

from opentf.tools.ctlworkflows import (
    workflow_cmd,
    print_workflow_help,
    _add_files,
    _add_variables,
    _generate_row,
    _get_outputformat,
    _get_first_page,
    _get_workflow_events,
    kill_workflow,
    list_workflows,
    run_workflow,
    get_workflow,
    get_qualitygate,
    _handler_maybe_outdated,
    _handler_maybe_details,
)
from opentf.tools.ctlnetworking import _could_not_connect, _get, _delete, _post

import opentf.tools.ctl


########################################################################
# Datasets

CONFIG = {
    'token': 'AAAABBBBCCCCDDDDEEEEFFFF0000111122223333',
    'orchestrator': {
        'insecure-skip-tls-verify': True,
        'ports': {
            'eventbus': 1234,
            'receptionist': 4321,
            'observer': 9999,
            'killswitch': 8888,
        },
        'server': 'aaaa',
    },
}

HEADERS = {'Authorization': 'Bearer ' + CONFIG['token']}

OBSERVER_WORKFLOWS_EMPTY = {'details': {'items': []}}
OBSERVER_WORKFLOWS = {'details': {'items': ['a1', 'b2']}}

OBSERVER_FIRST_PAGE_OK = {
    'details': {
        'items': [
            {
                'kind': 'Workflow',
                'metadata': {
                    'name': 'nAMe',
                    'creationTimestamp': '1970-01-23',
                },
            }
        ],
        'status': 'DONE',
    },
}

OBSERVER_FIRST_PAGE_RUNNING_OK = {
    'details': {
        'items': [
            {
                'kind': 'Workflow',
                'metadata': {
                    'name': 'nAMe',
                    'creationTimestamp': '1970-01-23',
                },
            }
        ],
        'status': 'RUNNING',
    },
}

OBSERVER_FIRST_PAGE_DONE_OK = {
    'details': {
        'items': [
            {
                'kind': 'Workflow',
                'metadata': {
                    'name': 'nAMe',
                    'creationTimestamp': '1970-01-23',
                },
            },
            {
                'kind': 'ExecutionCommand',
                'metadata': {
                    'name': 'executioncommand_nAMe',
                    'creationTimestamp': '1970-01-23',
                    'step_sequence_id': -1,
                    'job_id': 'executioncommand_job_id',
                },
                'runs-on': ['windows'],
            },
        ],
        'status': 'DONE',
    },
}

OBSERVER_SECOND_PAGE_OK = {
    'details': {
        'items': [
            {
                'kind': 'ExecutionCommand',
                'metadata': {
                    'name': 'executioncommand_nAMe',
                    'creationTimestamp': '1970-01-23',
                    'step_sequence_id': -1,
                    'job_id': 'executioncommand_job_id',
                },
                'runs-on': ['windows'],
            },
        ],
        'status': 'DONE',
    },
}

OBSERVER_FIRST_PAGE_FAILED = {
    'details': {
        'items': [
            {
                'kind': 'Workflow',
                'metadata': {
                    'name': 'nAMe',
                    'creationTimestamp': '1970-01-23',
                },
            }
        ],
        'status': 'FAILED',
    },
}

OBSERVER_FIRST_PAGE_SPURIOUS_OK = {
    'details': {
        'items': [
            {},
            {
                'kind': 'Workflow',
                'metadata': {
                    'name': 'nAMe',
                    'creationTimestamp': '1970-01-23',
                },
            },
            {
                'kind': 'ExecutionCommand',
                'metadata': {
                    'name': 'executioncommand_nAMe',
                    'creationTimestamp': '1970-01-23',
                    'step_sequence_id': -1,
                    'job_id': 'executioncommand_job_id',
                },
                'runs-on': ['windows'],
            },
            {
                'kind': 'ExecutionResult',
                'metadata': {
                    'name': 'nAMe',
                    'creationTimestamp': '1970-01-23',
                    'job_id': 'executionresults_job_id',
                },
                'status': 0,
                'logs': ['executionresult_log'],
            },
            {
                'kind': 'ExecutionCommand',
                'metadata': {
                    'name': 'executioncommand_nAMe2',
                    'creationTimestamp': '1970-01-23',
                    'step_sequence_id': -1,
                    'job_id': 'executioncommand_2_job_id',
                },
                'runs-on': ['windows'],
            },
            {
                'kind': 'ExecutionResult',
                'metadata': {
                    'name': 'nAMe',
                    'creationTimestamp': '1970-01-23',
                    'job_id': 'executionresults_2_job_id',
                },
                'status': 123,
                'logs': ['executionresult_2_log'],
                'attachments': ['foo'],
            },
            {
                'kind': 'ProviderCommand',
                'metadata': {
                    'name': 'nAMe',
                    'creationTimestamp': '1970-01-23',
                    'job_id': 'providercommand_job_id',
                },
            },
            {
                'kind': 'ExecutionError',
                'metadata': {
                    'name': 'nAMe',
                    'creationTimestamp': '1970-01-23',
                    'job_id': 'executionerror_job_id',
                },
            },
            {
                'kind': 'ExecutionCommand',
                'metadata': {
                    'name': 'executioncommand_nAMe3',
                    'creationTimestamp': '1970-01-23',
                    'step_sequence_id': -2,
                    'job_id': 'executioncommand_3_job_id',
                },
                'runs-on': ['windows'],
            },
            {
                'kind': 'ExecutionResult',
                'metadata': {
                    'name': 'nAMe_release',
                    'creationTimestamp': '1970-01-23',
                    'job_id': 'executionresults_2_job_id',
                },
                'status': 456,
                'logs': ['executionresult_3_log'],
            },
        ],
        'status': 'RUNNING',
    },
}

OBSERVER_FIRST_PAGE_SPURIOUS_NOCTS_OK = {
    'details': {
        'items': [
            {},
            {
                'kind': 'Workflow',
                'metadata': {
                    'name': 'nAMe',
                    'creationTimestamp': '1970-01-23',
                },
            },
            {
                'kind': 'ExecutionCommand',
                'metadata': {
                    'name': 'executioncommand_nAMe',
                    'creationTimestamp': '1970-01-23',
                    'step_sequence_id': -1,
                    'job_id': 'executioncommand_job_id',
                },
                'runs-on': ['windows'],
            },
            {
                'kind': 'ExecutionResult',
                'metadata': {
                    'name': 'nAMe',
                    'creationTimestamp': '1970-01-23',
                    'job_id': 'executionresults_job_id',
                },
                'status': 0,
                'logs': ['executionresult_log'],
            },
            {
                'kind': 'ExecutionCommand',
                'metadata': {
                    'name': 'executioncommand_nAMe2',
                    'creationTimestamp': '1970-01-23',
                    'step_sequence_id': -1,
                    'job_id': 'executioncommand_2_job_id',
                },
                'runs-on': ['windows'],
            },
            {
                'kind': 'ExecutionResult',
                'metadata': {
                    'name': 'nAMe',
                    'creationTimestamp': '1970-01-23',
                    'job_id': 'executionresults_2_job_id',
                },
                'status': 123,
                'logs': ['executionresult_2_log'],
                'attachments': ['foo'],
            },
            {
                'kind': 'ProviderCommand',
                'metadata': {
                    'name': 'nAMe',
                    'creationTimestamp': '1970-01-23',
                    'job_id': 'providercommand_job_id',
                },
            },
            {
                'kind': 'ExecutionError',
                'metadata': {
                    'name': 'nAMe',
                    'workflow_id': 'abc-id',
                },
                'details': {
                    'foo.bar': 'FOO.bAR',
                    'bar.baZ': 'BAR.BAz',
                },
            },
            {
                'kind': 'ExecutionCommand',
                'metadata': {
                    'name': 'executioncommand_nAMe3',
                    'creationTimestamp': '1970-01-23',
                    'step_sequence_id': -2,
                    'job_id': 'executioncommand_3_job_id',
                },
                'runs-on': ['windows'],
            },
            {
                'kind': 'ExecutionResult',
                'metadata': {
                    'name': 'nAMe_release',
                    'creationTimestamp': '1970-01-23',
                    'job_id': 'executionresults_2_job_id',
                },
                'status': 456,
                'logs': ['executionresult_3_log'],
            },
        ],
        'status': 'RUNNING',
    },
}

OBSERVER_FIRST_PAGE_SPURIOUS_MISSING_OK = {
    'details': {
        'items': [
            {
                'kind': 'ExecutionCommand',
                'metadata': {
                    'name': 'executioncommand_nAMe',
                    'creationTimestamp': '1970-01-23',
                    'step_sequence_id': -1,
                    'job_id': 'executioncommand_job_id',
                },
                'runs-on': ['windows'],
            },
            {
                'kind': 'ExecutionResult',
                'metadata': {
                    'name': 'nAMe',
                    'creationTimestamp': '1970-01-23',
                    'job_id': 'executionresults_job_id',
                },
                'status': 0,
                'logs': ['executionresult_log'],
            },
            {
                'kind': 'ExecutionCommand',
                'metadata': {
                    'name': 'executioncommand_nAMe2',
                    'creationTimestamp': '1970-01-23',
                    'step_sequence_id': -1,
                    'job_id': 'executioncommand_2_job_id',
                },
                'runs-on': ['windows'],
            },
            {
                'kind': 'ExecutionResult',
                'metadata': {
                    'name': 'nAMe',
                    'creationTimestamp': '1970-01-23',
                    'job_id': 'executionresults_2_job_id',
                },
                'status': 123,
                'logs': ['executionresult_2_log'],
                'attachments': ['foo'],
            },
            {
                'kind': 'ProviderCommand',
                'metadata': {
                    'name': 'nAMe',
                    'creationTimestamp': '1970-01-23',
                    'job_id': 'providercommand_job_id',
                },
            },
            {
                'kind': 'ExecutionError',
                'metadata': {
                    'name': 'nAMe',
                    'creationTimestamp': '1970-01-23',
                    'job_id': 'executionerror_job_id',
                },
            },
            {
                'kind': 'ExecutionCommand',
                'metadata': {
                    'name': 'executioncommand_nAMe3',
                    'creationTimestamp': '1970-01-23',
                    'step_sequence_id': -2,
                    'job_id': 'executioncommand_3_job_id',
                },
                'runs-on': ['windows'],
            },
            {
                'kind': 'ExecutionResult',
                'metadata': {
                    'name': 'nAMe_release',
                    'creationTimestamp': '1970-01-23',
                    'job_id': 'executionresults_2_job_id',
                },
                'status': 456,
                'logs': ['executionresult_3_log'],
            },
        ],
        'status': 'RUNNING',
    },
}

SUBSCRIPTION_COLUMNS = (
    'NAME:.metadata.name',
    'CREATION:.metadata.creationTimestamp',
    'ENDPOINT:.spec.subscriber.endpoint',
)

SUBSCRIPTION_COLUMNS_FULL = (
    'NAME:.metadata.name',
    'CREATION:.metadata.creationTimestamp',
    'ENDPOINT:.spec.subscriber.endpoint',
    'PC:.status.publicationCount',
    'ANNO:.metadata.annotations',
)

SUBSCRIPTION_COLUMNS_FULL2 = (
    'ID:.metadata.subscription_id',
    'NAME:.metadata.name',
    'CREATION:.metadata.creationTimestamp',
    'ENDPOINT:.spec.subscriber.endpoint',
    'PC:.status.publicationCount',
    'ANNO:.metadata.annotations',
)

SUBSCRIPTION_MANIFEST = {
    'metadata': {
        'name': 'nAMe',
        'creationTimestamp': 'tIMeSTAMp',
        'annotations': {},
    },
    'spec': {'subscriber': {'endpoint': 'eNDpOINt'}},
    'status': {'publicationCount': 123},
}

AGENTREGISTRATION_MANIFEST_IDLE = {
    'apiVersion': 'opentestfactory.org/v1alpha1',
    'kind': 'AgentRegistration',
    'metadata': {
        'name': 'aGENtREgistration',
        'creationTimestamp': 'crEAtioNTimestamp',
        'namespaces': '*',
    },
    'spec': {
        'encoding': 'utf-8',
        'script_path': 'scRIPt_pAtH',
        'tags': ['windows', 'roboTFRamework'],
    },
    'status': {
        'communicationCount': 12,
        'communicationStatusSummary': {},
        'lastCommunicationTimestamp': 'laSTCOMmunicationTimestamp',
    },
}
AGENTREGISTRATION_MANIFEST_BUSY = {
    'apiVersion': 'opentestfactory.org/v1alpha1',
    'kind': 'AgentRegistration',
    'metadata': {
        'name': 'aGENtREgistration',
        'creationTimestamp': 'crEAtioNTimestamp',
        'namespaces': 'a,b',
    },
    'spec': {
        'encoding': 'utf-8',
        'script_path': 'scRIPt_pAtH',
        'tags': ['windows', 'roboTFRamework'],
    },
    'status': {
        'communicationCount': 12,
        'communicationStatusSummary': {},
        'lastCommunicationTimestamp': 'laSTCOMmunicationTimestamp',
        'currentJobID': 'a12B34c56',
    },
}

AGENTREGISTRATION_COLUMNS = (
    'NAME:.metadata.name',
    'AGENT_ID:.metadata.agent_id',
    'TAGS:.spec.tags',
    'REGISTRATION_TIMESTAMP:.metadata.creationTimestamp',
    'LAST_SEEN_TIMESTAMP:.status.lastCommunicationTimestamp',
    'RUNNING_JOB:.status.currentJobID',
)

UUID_OK = '6120ee73-2296-4dc6-9b3b-b6f2d4b4a622'
UUID_BAD = 'z6120ee73-2296-4dc6-9b3b-b6f2d4b4a622'

CHANNELS_COLUMNS = {'JOB:.status.currentJobID', 'PROVIDER:.metadata.channelhandler_id'}

CHANNEL_MANIFEST_IDLE = {
    'apiVersion': 'opentestfactory.org/v1alpha1',
    'kind': 'Channel',
    'metadata': {
        'name': 'foo',
        'namespaces': 'bar',
        'channelhandler_id': 'uuid',
    },
    'spec': {'tags': ['a', 'b', 'c']},
    'status': {
        'lastCommunicationTimestamp': 'atimestamp',
        'phase': 'IDLE',
        'currentJobID': None,
    },
}

CHANNEL_MANIFEST_BUSY = {
    'apiVersion': 'opentestfactory.org/v1alpha1',
    'kind': 'Channel',
    'metadata': {
        'name': 'foo',
        'namespaces': 'bar,foo',
        'channelhandler_id': 'uuid',
    },
    'spec': {'tags': ['a', 'b', 'c']},
    'status': {
        'lastCommunicationTimestamp': 'atimestamp',
        'phase': 'BUSY',
        'currentJobID': UUID_OK,
    },
}

CHANNEL_MANIFEST_PENDING = {
    'apiVersion': 'opentestfactory.org/v1alpha1',
    'kind': 'Channel',
    'metadata': {
        'name': 'foo',
        'namespaces': '*',
        'channelhandler_id': 'uuid',
    },
    'spec': {'tags': ['a', 'b', 'c']},
    'status': {
        'lastCommunicationTimestamp': 'atimestamp',
        'phase': 'PENDING',
        'currentJobID': UUID_OK,
    },
}

TOKEN = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzUxMiJ9.eyJpc3MiOiJleGFtcGxlLmNvbSIsInN1YiI6Im5vYm9keSJ9.ABsPbmeakwzxS-brB8BZcqMMonqolTCG6PJwcxACSMkl7lLURocEXzybLxhhlarQuCFt3wpZpEeDRoXjlLketl-ZruzQQ889730KD1m1VvBBzwpulrrv9uWLY9TVjnC7r-j3Rhf758HDCpozFy-Q6fFP1mgDL_ZqVZVFOUWkQeI-dzwebzLfsz7Y_KZsuwTvimpjzQ8GePMwjp0sPVmEAuC8GDPiUFA2WbHJGrS1LQGSD5T9j8QsXnv_uaVpag5KFzfN0ZG0jFZe4UBiPi7Grq_WjAc8lA9GrAduV5m7SP8jl4DMA0ufeRAFLzdlg53qRPp8o2Lr1IjX_qBxp6ZrpIS_bjbkI41WEaHNreFK6Zh2eqz-Ms60RphYQw_hDSqlwc402WLGvwCururKCzDelKAXSS0HFlWPS4cY7wHDJmRy76XOYNrsxoAAg__Ci1FspzWw8ngNvkMnwuQpRrYsOhQ4W0orNTpYtQWXdt8HKm2fb-lXe5Rmxev9lZnqKe77HNET8ITKPz7pC_ZVCcV4GneMY3ryzUAMWMM2bBzTkjvWSCMxSCMa7vptL-nCSZro8H1GYzWY9PAW0ZU5Mj7-iYO1A0ciHnDkJziQNRTksD30qt45se1ot8G4Kt8P6vxxsi-mgAjYnZGOH_g5EW54q806cMq4vJ-DLn-Llov3Y3s'

########################################################################
# Helpers

mockresponse = Response()
mockresponse.status_code = 200


def _make_response(status_code, json_value):
    mock = Response()
    mock.status_code = status_code
    mock.json = lambda: json_value  # pyright: ignore
    return mock


class TestCtl(unittest.TestCase):
    """Unit tests for ctlworkflows, ctlnetworking & ctl."""

    ## ctlworkflows

    # workflow_cmd

    def test_workflow_cmd_1(self):
        args = ['', 'workflow', 'unknown']
        with patch.object(sys, 'argv', args):
            self.assertRaises(SystemExit, workflow_cmd)

    def test_workflow_cmd_2(self):
        args = ['', 'get', 'workflows']
        mock = MagicMock(return_value=OBSERVER_WORKFLOWS_EMPTY)
        mock2 = MagicMock()
        with patch('opentf.tools.ctlworkflows._get', mock), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch.object(
            sys, 'argv', args
        ), patch(
            'opentf.tools.ctlworkflows.read_configuration', mock2
        ):
            workflow_cmd()
        mock.assert_called_once()
        mock2.assert_called_once()

    def test_workflow_cmd_3(self):
        args = ['', 'get', 'qualitygate', UUID_OK]
        mock = MagicMock(return_value={'code': 200, 'details': {'status': 'NOTEST'}})
        mock2 = MagicMock()
        with patch('opentf.tools.ctlworkflows._get', mock), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch.object(
            sys, 'argv', args
        ), patch(
            'opentf.tools.ctlworkflows.read_configuration', mock2
        ):
            workflow_cmd()
        mock.assert_called_once()
        mock2.assert_called_once()

    def test_workflow_cmd_4(self):
        args = ['', 'get', 'qualitygate', UUID_OK, '--mode=yada']
        mock = MagicMock(return_value={'code': 422, 'message': 'bad mode'})
        mock2 = MagicMock()
        with patch('opentf.tools.ctlworkflows._get', mock), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch.object(
            sys, 'argv', args
        ), patch(
            'opentf.tools.ctlworkflows.read_configuration', mock2
        ):
            self.assertRaises(SystemExit, workflow_cmd)

    def test_workflow_cmd_5(self):
        args = ['', 'get', 'qualitygate', UUID_BAD]
        mock = MagicMock(return_value={'code': 404})
        mock2 = MagicMock()
        with patch('opentf.tools.ctlworkflows._get', mock), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch.object(
            sys, 'argv', args
        ), patch(
            'opentf.tools.ctlworkflows.read_configuration', mock2
        ):
            self.assertRaises(SystemExit, workflow_cmd)

    # print_workflow_help

    def test_print_workflow_help_1(self):
        args = ['', 'unknown']
        self.assertRaises(SystemExit, print_workflow_help, args)

    def test_print_workflow_help_2(self):
        args = ['', 'kill', 'workflow', '123']
        mock = MagicMock()
        with patch('builtins.print', mock):
            print_workflow_help(args)
        self.assertTrue(mock.call_count > 0)

    def test_print_workflow_help_3(self):
        args = ['', 'get', 'workflows']
        mock = MagicMock()
        with patch('builtins.print', mock):
            print_workflow_help(args)
        self.assertTrue(mock.call_count > 0)

    def test_print_workflow_help_4(self):
        args = ['', 'get', 'qualitygate']
        mock = MagicMock()
        with patch('builtins.print', mock):
            print_workflow_help(args)
        self.assertTrue(mock.call_count > 0)

    def test_print_workflow_help_5(self):
        args = ['', 'run', 'workflow']
        mock = MagicMock()
        with patch('builtins.print', mock):
            print_workflow_help(args)
        self.assertTrue(mock.call_count > 0)

    # _add_files

    def test_add_files(self):
        args = [
            'foo=bar',
            '-f',
            'yada=/foo/bar',
            '-f',
            'yoda=/bar/baz',
            '-e',
            'nono=noway',
        ]
        files = {}
        with patch('builtins.open', lambda x, y: 'DUMMY'):
            _add_files(args, files)
        self.assertEqual(len(files), 2)
        self.assertTrue('yada' in files)
        self.assertTrue('yoda' in files)

    # get_qualitygate

    def test_get_qualitygate_1(self):
        mock = MagicMock(return_value={'details': {'status': 'NOTEST'}})
        mock2 = MagicMock()
        with patch('opentf.tools.ctlworkflows._get', mock), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS):
            result = get_qualitygate(UUID_OK, 'mOdE')
        self.assertTrue('mode=mOdE' in mock.call_args_list[0][0][1])

    def test_get_qualitygate_2(self):
        mock = MagicMock(return_value={'details': {'status': 'UNEXPECTED'}})
        mock2 = MagicMock()
        mock_exit = MagicMock()
        mock_exit.side_effect = SystemExit()
        with patch('opentf.tools.ctlworkflows._get', mock), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch(
            'sys.exit', mock_exit
        ):
            self.assertRaises(SystemExit, get_qualitygate, UUID_OK, 'mOdE')
        self.assertTrue('mode=mOdE' in mock.call_args_list[0][0][1])
        self.assertEqual(mock_exit.call_args_list[0][0][0], 2)

    def test_get_qualitygate_3(self):
        mock = MagicMock(return_value={})
        mock2 = MagicMock()
        mock_exit = MagicMock()
        mock_exit.side_effect = SystemExit()
        with patch('opentf.tools.ctlworkflows._get', mock), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch(
            'sys.exit', mock_exit
        ):
            self.assertRaises(SystemExit, get_qualitygate, UUID_OK, 'mOdE')
        self.assertTrue('mode=mOdE' in mock.call_args_list[0][0][1])
        mock_exit.assert_called_once()
        self.assertEqual(mock_exit.call_args_list[0][0][0], 2)

    def test_get_qualitygate_4(self):
        mock = MagicMock(return_value={'details': {'status': 'RUNNING'}})
        mock2 = MagicMock()
        mock_exit = MagicMock()
        mock_exit.side_effect = SystemExit()
        with patch('opentf.tools.ctlworkflows._get', mock), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch(
            'sys.exit', mock_exit
        ):
            self.assertRaises(SystemExit, get_qualitygate, UUID_OK, 'mOdE')
        self.assertTrue('mode=mOdE' in mock.call_args_list[0][0][1])
        self.assertEqual(mock_exit.call_args_list[0][0][0], 101)

    def test_get_qualitygate_5(self):
        mock = MagicMock(return_value={'details': {'status': 'FAILURE'}})
        mock2 = MagicMock()
        mock_exit = MagicMock()
        mock_exit.side_effect = SystemExit()
        with patch('opentf.tools.ctlworkflows._get', mock), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch(
            'sys.exit', mock_exit
        ):
            self.assertRaises(SystemExit, get_qualitygate, UUID_OK, 'mOdE')
        self.assertTrue('mode=mOdE' in mock.call_args_list[0][0][1])
        self.assertEqual(mock_exit.call_args_list[0][0][0], 102)

    # _add_variables

    def test_add_variables_1(self):
        args = [
            'foo=bar',
            '-e',
            'yada=foo/bar',
            '-f',
            'yoda=/bar/baz',
            '-e',
            'nono=noway',
        ]
        files = {}
        _add_variables(args, files)
        self.assertEqual(len(files), 1)
        self.assertTrue('variables' in files)
        self.assertTrue('yada=foo/bar' in files['variables'])
        self.assertFalse('yoda=/bar/baz' in files['variables'])

    def test_add_variables_2(self):
        args = ['foo=bar', '-e', 'yada=foo/bar', '-f', 'yoda=/bar/baz', '-e', 'nono']
        files = {}
        with patch('builtins.open', mock_open(read_data='foo=bar\nbar=baz')):
            result = _add_variables(args, files)
        self.assertEqual(len(files), 1)
        self.assertTrue('variables' in files)
        self.assertTrue('yada=foo/bar' in files['variables'])
        self.assertTrue('foo=bar' in files['variables'])
        self.assertTrue('bar=baz' in files['variables'])

    def test_add_variables_3(self):
        args = ['foo=bar', '-e', 'yada=foo/bar', '-f', 'yoda=/bar/baz', '-e', 'nono']
        files = {}
        with patch('builtins.open', mock_open(read_data='foo=bar\nbar<-baz')):
            self.assertRaises(SystemExit, _add_variables, args, files)

    # _generate_row

    def test_generate_row_1(self):
        response = _generate_row('aaa', {}, [])
        self.assertEqual(len(response), 0)

    def test_generate_row_2(self):
        response = _generate_row(
            'aaa',
            OBSERVER_FIRST_PAGE_OK,
            [
                'name:.metadata.name',
                'start:.metadata.creationTimestamp',
                'status:.details.status',
            ],
        )
        self.assertEqual(len(response), 3)

    def test_generate_row_3(self):
        response = _generate_row(
            'aaa',
            OBSERVER_FIRST_PAGE_SPURIOUS_OK,
            [
                'name:.metadata.name',
                'wid:.metadata.workflow_id',
                'start:.metadata.creationTimestamp',
                'status:.details.status',
            ],
        )
        self.assertEqual(len(response), 4)

    def test_generate_row_4(self):
        response = _generate_row(
            'aaa',
            OBSERVER_FIRST_PAGE_SPURIOUS_OK,
            [
                'name:.metadata.name',
                'wid:.metadata.workflow_id',
                'start:.metadata.creationTimestamp',
                'status:.details.status',
            ],
        )
        self.assertEqual(len(response), 4)
        self.assertEqual(response[0], 'nAMe')

    def test_generate_row_5(self):
        response = _generate_row(
            'aaa',
            OBSERVER_FIRST_PAGE_SPURIOUS_MISSING_OK,
            [
                'name:.metadata.name',
                'wid:.metadata.workflow_id',
                'start:.metadata.creationTimestamp',
                'status:.details.status',
            ],
        )
        self.assertEqual(len(response), 4)
        self.assertEqual(response[0], '')

    # kill_workflow

    def test_kill_workflow_1(self):
        mockresponse = _make_response(200, None)
        mock = MagicMock(return_value=mockresponse)
        with patch('requests.delete', mock), patch('requests.get', mock), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS):
            kill_workflow(UUID_OK)

    def test_kill_workflow_2(self):
        mockresponse_get = _make_response(404, None)
        mock_get = MagicMock(return_value=mockresponse_get)
        mockresponse = _make_response(200, None)
        mock = MagicMock(return_value=mockresponse)
        with patch('requests.delete', mock), patch('requests.get', mock_get), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS):
            self.assertRaises(SystemExit, kill_workflow, UUID_OK)

    def test_kill_workflow_3(self):
        self.assertRaises(SystemExit, kill_workflow, UUID_BAD)

    # _get_first_page

    def test_get_first_page_1(self):
        mockresponse = Response()
        mockresponse.status_code = 404
        mock = MagicMock(return_value=mockresponse)
        with patch('requests.get', mock), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS):
            self.assertRaises(SystemExit, _get_first_page, 'a1b2c3')

    def test_get_first_page_2(self):
        mockresponse = Response()
        mockresponse.status_code = 500
        mock = MagicMock(return_value=mockresponse)
        with patch('requests.get', mock), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS):
            self.assertRaises(SystemExit, _get_first_page, 'a1b2c3')

    def test_get_first_page_3(self):
        mockresponse = _make_response(200, OBSERVER_FIRST_PAGE_OK)
        mock = MagicMock(return_value=mockresponse)
        with patch('requests.get', mock), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS):
            result = _get_first_page('a1b2c3')
        self.assertEqual(mock.call_count, 1)
        self.assertIsInstance(result, Response)

    # list_workflows

    def test_list_workflows_1(self):
        mock = MagicMock(return_value=OBSERVER_WORKFLOWS_EMPTY)
        with patch('opentf.tools.ctlworkflows._get', mock), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS):
            list_workflows()

    def test_list_workflows_2(self):
        args = ['', '--output=custom-columns=invalid']
        with patch('opentf.tools.ctlnetworking.CONFIG', CONFIG), patch(
            'opentf.tools.ctlnetworking.HEADERS', HEADERS
        ):
            self.assertRaises(SystemExit, list_workflows)

    def test_list_workflows_3(self):
        mock = MagicMock(return_value=OBSERVER_WORKFLOWS)
        mock2response = _make_response(200, OBSERVER_FIRST_PAGE_OK)
        mock2 = MagicMock(return_value=mock2response)
        with patch('opentf.tools.ctlworkflows._get', mock), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch(
            'opentf.tools.ctlworkflows._get_first_page', mock2
        ):
            list_workflows()
        self.assertEqual(mock2.call_count, 2)

    # run_workflow

    def test_run_workflow_1(self):
        mockresponse = _make_response(201, {'details': {'workflow_id': 'woRKflow_Id'}})
        mock = MagicMock(return_value=mockresponse)
        with patch('requests.post', mock), patch(
            'builtins.open', mock_open(read_data='{}')
        ), patch('opentf.tools.ctl._get', mock), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch(
            'opentf.tools.ctlnetworking.HEADERS', HEADERS
        ):
            run_workflow('fooBar')

    def test_run_workflow_2(self):
        mockresponse = _make_response(
            401,
            {
                'message': 'yada',
                'details': {'error': 'woRKflow'},
            },
        )
        mock = MagicMock(return_value=mockresponse)
        with patch('requests.post', mock), patch(
            'builtins.open', mock_open(read_data='{}')
        ):
            self.assertRaises(SystemExit, run_workflow, 'fooBar')

    # get_workflow

    def test_get_workflow_1(self):
        mock2response = _make_response(200, OBSERVER_FIRST_PAGE_OK)
        mock2 = MagicMock(return_value=mock2response)
        with patch('requests.get', mock2), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS):
            self.assertIsNone(get_workflow(UUID_OK))
        self.assertEqual(mock2.call_count, 2)

    def test_get_workflow_2(self):
        mock_get_response = _make_response(200, OBSERVER_FIRST_PAGE_SPURIOUS_OK)
        mock_get = MagicMock(return_value=mock_get_response)
        with patch('requests.get', mock_get), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS):
            self.assertIsNone(get_workflow(UUID_OK))
        self.assertEqual(mock_get.call_count, 2)

    def test_get_workflow_3(self):
        mock_get_response = _make_response(200, OBSERVER_FIRST_PAGE_FAILED)
        mock_get = MagicMock(return_value=mock_get_response)
        with patch('requests.get', mock_get), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS):
            self.assertIsNone(get_workflow(UUID_OK))
        self.assertEqual(mock_get.call_count, 2)

    def test_get_workflow_4(self):
        args = ['', '-o', 'json']
        mock_get_response = _make_response(200, OBSERVER_FIRST_PAGE_FAILED)
        mock_get = MagicMock(return_value=mock_get_response)
        mock_print = MagicMock()
        with patch('requests.get', mock_get), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch.object(
            sys, 'argv', args
        ), patch(
            'builtins.print', mock_print
        ):
            self.assertIsNone(get_workflow(UUID_OK))
        self.assertEqual(mock_get.call_count, 2)

    def test_get_workflow_5(self):
        args = ['', '--output=yaml']
        mock_get_response = _make_response(200, OBSERVER_FIRST_PAGE_FAILED)
        mock_get = MagicMock(return_value=mock_get_response)
        mock_print = MagicMock()
        with patch('requests.get', mock_get), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch.object(
            sys, 'argv', args
        ), patch(
            'builtins.print', mock_print
        ):
            self.assertIsNone(get_workflow(UUID_OK))
        self.assertEqual(mock_get.call_count, 2)

    def test_get_workflow_missing_creationtimestamp(self):
        mock_get_response = _make_response(200, OBSERVER_FIRST_PAGE_SPURIOUS_NOCTS_OK)
        mock_get = MagicMock(return_value=mock_get_response)
        with patch('requests.get', mock_get), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS):
            result = get_workflow(UUID_OK)
        self.assertEqual(mock_get.call_count, 2)

    # _get_outputformat

    def test_get_outputformat_1(self):
        args = ['', '-o', 'xml']
        with patch.object(sys, 'argv', args):
            self.assertRaises(SystemExit, _get_outputformat, allowed=('json', 'yaml'))

    def test_get_outputformat_2(self):
        args = ['', '-a', '-o']
        with patch.object(sys, 'argv', args):
            self.assertRaises(SystemExit, _get_outputformat, allowed=('json', 'yaml'))

    def test_get_outputformat_3(self):
        args = ['', '-o', 'json', 'foo']
        with patch.object(sys, 'argv', args):
            result = _get_outputformat(allowed=('json', 'yaml'))
        self.assertEqual(result, 'json')

    def test_get_outputformat_4(self):
        args = ['', '--output=json', 'foo']
        with patch.object(sys, 'argv', args):
            result = _get_outputformat(allowed=('json', 'yaml'))
        self.assertEqual(result, 'json')

    def test_get_outputformat_5(self):
        args = ['', '-o=json', 'foo']
        with patch.object(sys, 'argv', args):
            result = _get_outputformat(allowed=('json', 'yaml'))
        self.assertEqual(result, 'json')

    # handlers

    def test_handler_maybe_outdated_1(self):
        mockresponse = Response()
        mockresponse.status_code = 405
        mock_error = MagicMock()
        mock_debug = MagicMock()
        with patch('opentf.tools.ctlworkflows._error', mock_error), patch(
            'opentf.tools.ctlworkflows._debug', mock_debug
        ):
            self.assertRaises(SystemExit, _handler_maybe_outdated, mockresponse)
        mock_error.assert_called_once()
        mock_debug.assert_called_once()

    def test_handler_maybe_outdated_2(self):
        mockresponse = Response()
        mockresponse.status_code = 500
        mock_error = MagicMock()
        mock_debug = MagicMock()
        with patch('opentf.tools.ctlworkflows._error', mock_error), patch(
            'opentf.tools.ctlworkflows._debug', mock_debug
        ):
            self.assertRaises(SystemExit, _handler_maybe_outdated, mockresponse)
        mock_error.assert_called_once()
        mock_debug.assert_not_called()

    def test_handler_maybe_details_1(self):
        mockresponse = _make_response(500, {'message': 'meSSagE'})
        mock_error = MagicMock()
        with patch('opentf.tools.ctlworkflows._error', mock_error):
            self.assertRaises(SystemExit, _handler_maybe_details, mockresponse)
        mock_error.assert_called_once()

    def test_handler_maybe_details_2(self):
        mockresponse = _make_response(
            500,
            {'message': 'meSSagE', 'details': {'error': 'eRRoR'}},
        )
        mock_error = MagicMock()
        with patch('opentf.tools.ctlworkflows._error', mock_error):
            self.assertRaises(SystemExit, _handler_maybe_details, mockresponse)
        self.assertEqual(mock_error.call_count, 2)

    # ctlnetworking

    # _could_not_connect

    def test_could_not_connect_1(self):
        self.assertRaises(
            SystemExit, _could_not_connect, 'foo', exceptions.ProxyError('oops')
        )

    def test_could_not_connect_2(self):
        self.assertRaises(
            SystemExit, _could_not_connect, 'foo', exceptions.SSLError('oops')
        )

    # _get

    def test_get_1(self):
        mockresponse = _make_response(200, {})
        mock = MagicMock(return_value=mockresponse)
        with patch('requests.get', mock), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS):
            response = _get('foo')
        self.assertEqual(response, {})

    def test_get_2(self):
        mockresponse = _make_response(404, {})
        mock = MagicMock(return_value=mockresponse)
        with patch('requests.get', mock), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS):
            self.assertRaises(SystemExit, _get, 'foo')

    def test_get_3(self):
        mockresponse = _make_response(404, {})
        mock = MagicMock(return_value=mockresponse)
        mock2 = MagicMock()
        with patch('requests.get', mock), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS):
            response = _get('foo', handler=mock2)
        self.assertEqual(mock2.call_count, 1)

    # _delete

    def test_delete_1(self):
        mockresponse = _make_response(200, {})
        mock = MagicMock(return_value=mockresponse)
        with patch('requests.delete', mock), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS):
            response = _delete('foo')
        self.assertEqual(response, {})
        self.assertEqual(mock.call_count, 1)

    def test_delete_2(self):
        mockresponse = _make_response(404, {})
        mock = MagicMock(return_value=mockresponse)
        with patch('requests.delete', mock), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS):
            self.assertRaises(SystemExit, _delete, 'foo')

    def test_delete_3(self):
        mockresponse = _make_response(404, {})
        mock = MagicMock(return_value=mockresponse)
        mock2 = MagicMock()
        with patch('requests.delete', mock), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS):
            response = _delete('foo', handler=mock2)
        self.assertEqual(mock2.call_count, 1)

    # _post

    def test_post_1(self):
        mockresponse = _make_response(200, {})
        mock = MagicMock(return_value=mockresponse)
        with patch('requests.post', mock), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS):
            response = _post('foo')
        self.assertEqual(response, {})

    def test_post_2(self):
        mockresponse = _make_response(404, {})
        mock = MagicMock(return_value=mockresponse)
        with patch('requests.post', mock), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS):
            self.assertRaises(SystemExit, _post, 'foo')

    def test_post_3(self):
        mockresponse = _make_response(404, {})
        mock = MagicMock(return_value=mockresponse)
        mock2 = MagicMock()
        with patch('requests.post', mock), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS):
            response = _post('foo', handler=mock2)
        self.assertEqual(mock2.call_count, 1)

    # ctl

    # _generate_subscription_row

    def test_generate_subscription_row_1(self):
        response = opentf.tools.ctl._generate_subscription_row(
            SUBSCRIPTION_MANIFEST,
            SUBSCRIPTION_COLUMNS,
        )
        self.assertEqual(len(response), 3)

    def test_generate_subscription_row_2(self):
        response = opentf.tools.ctl._generate_subscription_row(
            SUBSCRIPTION_MANIFEST,
            SUBSCRIPTION_COLUMNS_FULL,
        )
        self.assertEqual(len(response), 5)

    def test_generate_subscription_row_3(self):
        response = opentf.tools.ctl._generate_subscription_row(
            SUBSCRIPTION_MANIFEST,
            SUBSCRIPTION_COLUMNS_FULL2,
        )
        self.assertEqual(len(response), 6)

    # list_subscriptions

    def test_list_subscriptions(self):
        mockresponse = {'items': {'a1b2c3': SUBSCRIPTION_MANIFEST}}
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.tools.ctl._get', mock), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS):
            result = opentf.tools.ctl.list_subscriptions()
        self.assertIsNone(result)

    # _generate_agent_row

    def test_generate_agent_row_1(self):
        AGENTREGISTRATION_MANIFEST_IDLE['metadata']['agent_id'] = 'iDLEaGenT_ID'
        response = opentf.tools.ctl._generate_agent_row(
            AGENTREGISTRATION_MANIFEST_IDLE,
            AGENTREGISTRATION_COLUMNS,
        )
        self.assertEqual(len(response), 6)
        self.assertFalse(response[-1])

    def test_generate_agent_row_2(self):
        AGENTREGISTRATION_MANIFEST_BUSY['metadata']['agent_id'] = 'bUSYaGenT_ID'
        response = opentf.tools.ctl._generate_agent_row(
            AGENTREGISTRATION_MANIFEST_BUSY,
            AGENTREGISTRATION_COLUMNS,
        )
        self.assertEqual(len(response), 6)
        self.assertTrue(response[-1])

    # list_agents

    def test_list_agents_old(self):
        mockresponse = {'items': {'a1b2c3': AGENTREGISTRATION_MANIFEST_BUSY}}
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.tools.ctl._get', mock), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS):
            result = opentf.tools.ctl.list_agents()
        self.assertIsNone(result)

    def test_list_agents_new(self):
        mockresponse = {'items': [AGENTREGISTRATION_MANIFEST_BUSY]}
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.tools.ctl._get', mock), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS):
            result = opentf.tools.ctl.list_agents()
        self.assertIsNone(result)

    # _generate_channel_row

    def test_generate_channel_row_idle(self):
        response = opentf.tools.ctl._generate_channel_row(
            CHANNEL_MANIFEST_IDLE,
            opentf.tools.ctl.CHANNEL_COLUMNS,
        )
        self.assertEqual(len(response), 5)
        self.assertEqual(response[-1], 'IDLE')

    def test_generate_channel_row_busy(self):
        response = opentf.tools.ctl._generate_channel_row(
            CHANNEL_MANIFEST_BUSY,
            opentf.tools.ctl.CHANNEL_COLUMNS,
        )
        self.assertEqual(len(response), 5)
        self.assertEqual(response[-1], 'BUSY')

    def test_generate_channel_row_pending(self):
        response = opentf.tools.ctl._generate_channel_row(
            CHANNEL_MANIFEST_PENDING,
            CHANNELS_COLUMNS,
        )
        self.assertEqual(len(response), 2)
        self.assertIsNotNone(response[-1])

    # list_channels

    def test_list_channels(self):
        mockresponse = {
            'details': {
                'items': [
                    CHANNEL_MANIFEST_BUSY,
                    CHANNEL_MANIFEST_BUSY,
                    CHANNEL_MANIFEST_PENDING,
                ]
            }
        }
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.tools.ctl._get', mock), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS):
            result = opentf.tools.ctl.list_channels()
        self.assertIsNone(result)

    # main

    def test_main_1(self):
        args = ['', 'unknown']
        with patch.object(sys, 'argv', args):
            self.assertRaises(SystemExit, opentf.tools.ctl.main)

    def test_main_2(self):
        args = ['', '--help']
        mock = MagicMock()
        with patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctl.print_help', mock
        ):
            self.assertRaises(SystemExit, opentf.tools.ctl.main)
        mock.assert_called_once()

    def test_main_2bis(self):
        args = ['']
        mock = MagicMock()
        with patch.object(sys, 'argv', args), patch('builtins.print', mock):
            self.assertRaises(SystemExit, opentf.tools.ctl.main)
        mock.assert_called_once()

    def test_main_3(self):
        args = ['', 'options']
        mock = MagicMock()
        with patch.object(sys, 'argv', args), patch('builtins.print', mock):
            self.assertRaises(SystemExit, opentf.tools.ctl.main)
        mock.assert_called_once()

    def test_main_4(self):
        args = ['', 'version']
        mock = MagicMock()
        with patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctl.get_tools_version', mock
        ):
            self.assertRaises(SystemExit, opentf.tools.ctl.main)
        mock.assert_called_once()

    def test_main_5(self):
        args = ['', 'get', 'subscriptions']
        mock = MagicMock()
        mock2 = MagicMock()
        with patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctl.list_subscriptions', mock
        ), patch('opentf.tools.ctl.read_configuration', mock2), patch(
            'opentf.tools.ctl._get', mock
        ), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch(
            'opentf.tools.ctlnetworking.HEADERS', HEADERS
        ):
            opentf.tools.ctl.main()
        mock.assert_called_once()

    def test_main_get_channels(self):
        args = ['', 'get', 'channels']
        mock = MagicMock()
        mock2 = MagicMock()
        with patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctl.list_channels', mock
        ), patch('opentf.tools.ctl.read_configuration', mock2), patch(
            'opentf.tools.ctl._get', mock2
        ), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch(
            'opentf.tools.ctlnetworking.HEADERS', HEADERS
        ):
            opentf.tools.ctl.main()
        mock.assert_called_once()

    def test_main_6(self):
        args = ['', 'get', 'agents']
        mock = MagicMock()
        mock2 = MagicMock()
        with patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctl.list_agents', mock
        ), patch('opentf.tools.ctl.read_configuration', mock2), patch(
            'opentf.tools.ctl._get', mock2
        ), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch(
            'opentf.tools.ctlnetworking.HEADERS', HEADERS
        ):
            opentf.tools.ctl.main()
        mock.assert_called_once()

    def test_main_7(self):
        agent_id = 'a1234-cafe'
        args = ['', 'delete', 'agent', agent_id]
        mock = MagicMock()
        mock2 = MagicMock()
        with patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctl.delete_agent', mock
        ), patch('opentf.tools.ctl.read_configuration', mock2), patch(
            'opentf.tools.ctl._get', mock2
        ), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch(
            'opentf.tools.ctlnetworking.HEADERS', HEADERS
        ):
            opentf.tools.ctl.main()
        mock.assert_called_once_with(agent_id)

    def test_main_8(self):
        key_id = 'kEy_Id'
        args = ['', 'generate', 'token', 'using', key_id]
        mock = MagicMock()
        mock2 = MagicMock()
        with patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctl.generate_token', mock
        ), patch('opentf.tools.ctl.read_configuration', mock2), patch(
            'opentf.tools.ctl._get', mock2
        ), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch(
            'opentf.tools.ctlnetworking.HEADERS', HEADERS
        ):
            self.assertRaises(SystemExit, opentf.tools.ctl.main)
        mock.assert_called_once_with(key_id)

    def test_main_8_bis(self):
        token = 'tOKen'
        args = ['', 'view', 'token', token]
        mock = MagicMock()
        with patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctl.view_token', mock
        ):
            self.assertRaises(SystemExit, opentf.tools.ctl.main)
        mock.assert_called_once_with(token)

    def test_main_8_ter(self):
        key_id = 'kEy_Id'
        args = ['', 'check', 'token', 'tOKen', 'using', key_id]
        mock = MagicMock()
        with patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctl.check_token', mock
        ):
            self.assertRaises(SystemExit, opentf.tools.ctl.main)
        mock.assert_called_once_with('tOKen', key_id)

    def test_main_9(self):
        args = ['', 'config', 'view']
        mock = MagicMock()
        mock2 = MagicMock()
        with patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctl.config_cmd', mock
        ), patch('opentf.tools.ctl.read_configuration', mock2), patch(
            'opentf.tools.ctl._get', mock2
        ), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch(
            'opentf.tools.ctlnetworking.HEADERS', HEADERS
        ):
            opentf.tools.ctl.main()
        mock.assert_called_once()

    def test_main_10(self):
        args = ['', 'get', 'workflows']
        mock = MagicMock()
        mock2 = MagicMock()
        with patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctl.workflow_cmd', mock
        ), patch('opentf.tools.ctl.read_configuration', mock2), patch(
            'opentf.tools.ctl._get', mock2
        ), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch(
            'opentf.tools.ctlnetworking.HEADERS', HEADERS
        ):
            opentf.tools.ctl.main()
        mock.assert_called_once()

    def test_main_11(self):
        args = ['', 'get', 'workflow']
        mock = MagicMock()
        mock2 = MagicMock()
        with patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctl.workflow_cmd', mock
        ), patch('opentf.tools.ctl.read_configuration', mock2), patch(
            'opentf.tools.ctl._get', mock2
        ), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch(
            'opentf.tools.ctlnetworking.HEADERS', HEADERS
        ):
            opentf.tools.ctl.main()
        mock.assert_called_once()

    def test_main_12(self):
        args = ['', 'get', 'qualitygate']
        mock = MagicMock()
        mock2 = MagicMock()
        with patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctl.workflow_cmd', mock
        ), patch('opentf.tools.ctl.read_configuration', mock2), patch(
            'opentf.tools.ctl._get', mock2
        ), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch(
            'opentf.tools.ctlnetworking.HEADERS', HEADERS
        ):
            opentf.tools.ctl.main()
        mock.assert_called_once()

    # print_help

    def test_print_help_1(self):
        args = ['', 'unknown', '--help']
        self.assertRaises(SystemExit, opentf.tools.ctl.print_help, args)

    def test_print_help_2(self):
        args = ['', 'delete', 'agent']
        mock = MagicMock()
        with patch('builtins.print', mock):
            opentf.tools.ctl.print_help(args)
        self.assertTrue(mock.call_count > 0)

    def test_print_help_3(self):
        args = ['', 'options']
        mock = MagicMock()
        with patch('builtins.print', mock):
            opentf.tools.ctl.print_help(args)
        self.assertTrue(mock.call_count > 0)

    def test_print_help_4(self):
        args = ['', 'version']
        mock = MagicMock()
        with patch('builtins.print', mock):
            opentf.tools.ctl.print_help(args)
        self.assertTrue(mock.call_count > 0)

    def test_print_help_5(self):
        args = ['', 'get', 'subscriptions']
        mock = MagicMock()
        with patch('builtins.print', mock):
            opentf.tools.ctl.print_help(args)
        self.assertTrue(mock.call_count > 0)

    def test_print_help_6(self):
        args = ['', 'generate', 'token']
        mock = MagicMock()
        with patch('builtins.print', mock):
            opentf.tools.ctl.print_help(args)
        self.assertTrue(mock.call_count > 0)

    def test_print_help_7(self):
        args = ['', 'get', 'agents']
        mock = MagicMock()
        with patch('builtins.print', mock):
            opentf.tools.ctl.print_help(args)
        self.assertTrue(mock.call_count > 0)

    def test_print_help_get_channels(self):
        args = ['', 'get', 'channels']
        mock = MagicMock()
        with patch('builtins.print', mock):
            opentf.tools.ctl.print_help(args)
        self.assertTrue(mock.call_count > 0)

    def test_print_help_8(self):
        args = ['', 'config', 'something']
        mock = MagicMock()
        with patch('opentf.tools.ctl.print_config_help', mock):
            opentf.tools.ctl.print_help(args)
        mock.assert_called_once()

    def test_print_help_9(self):
        args = ['', 'get', 'workflow']
        mock = MagicMock()
        with patch('opentf.tools.ctl.print_workflow_help', mock):
            opentf.tools.ctl.print_help(args)
        mock.assert_called_once()

    def test_print_help_9bis(self):
        args = ['', 'get', 'workflows']
        mock = MagicMock()
        with patch('opentf.tools.ctl.print_workflow_help', mock):
            opentf.tools.ctl.print_help(args)
        mock.assert_called_once()

    def test_print_help_10(self):
        args = ['', 'get', 'qualitygate']
        mock = MagicMock()
        with patch('opentf.tools.ctl.print_workflow_help', mock):
            opentf.tools.ctl.print_help(args)
        mock.assert_called_once()

    def test_print_help_11(self):
        args = ['', 'view', 'token']
        mock = MagicMock()
        with patch('builtins.print', mock):
            opentf.tools.ctl.print_help(args)
        mock.assert_called_once()

    def test_print_help_12(self):
        args = ['', 'check', 'token']
        mock = MagicMock()
        with patch('builtins.print', mock):
            opentf.tools.ctl.print_help(args)
        mock.assert_called_once()

    # tokens

    def test_view_token_1(self):
        self.assertIsNone(opentf.tools.ctl.view_token(TOKEN))

    def test_view_token_2(self):
        self.assertRaises(SystemExit, opentf.tools.ctl.view_token, 'bAD')

    def test_check_token_1(self):
        self.assertRaises(SystemExit, opentf.tools.ctl.check_token, TOKEN, '')

    def test_check_token_2(self):
        def _generate_token():
            """Generate temporary key and JWT token."""
            # pylint: disable=import-outside-toplevel
            from cryptography.hazmat.primitives import serialization
            from cryptography.hazmat.primitives.asymmetric import rsa
            from cryptography.hazmat.backends import default_backend

            private_key = rsa.generate_private_key(
                public_exponent=65537, key_size=4096, backend=default_backend()
            )
            public_key = private_key.public_key()
            pem = private_key.private_bytes(
                encoding=serialization.Encoding.PEM,
                format=serialization.PrivateFormat.TraditionalOpenSSL,
                encryption_algorithm=serialization.NoEncryption(),
            ).decode()
            pub = public_key.public_bytes(
                encoding=serialization.Encoding.PEM,
                format=serialization.PublicFormat.SubjectPublicKeyInfo,
            )
            token = jwt.encode(
                {'iss': 'check_token_unittest', 'sub': 'temp token'},
                pem,
                algorithm='RS512',
            )
            return token, str(pub, encoding='utf-8')

        token, pub = _generate_token()
        with patch('builtins.open', mock_open(read_data=pub)):
            response = opentf.tools.ctl.check_token(token, 'pUbliCKey')
        self.assertIsNone(response)

    def test_check_token_3(self):
        def _generate_token():
            """Generate temporary key and JWT token."""
            # pylint: disable=import-outside-toplevel
            from cryptography.hazmat.primitives import serialization
            from cryptography.hazmat.primitives.asymmetric import rsa
            from cryptography.hazmat.backends import default_backend

            private_key = rsa.generate_private_key(
                public_exponent=65537, key_size=4096, backend=default_backend()
            )
            public_key = private_key.public_key()
            pem = private_key.private_bytes(
                encoding=serialization.Encoding.PEM,
                format=serialization.PrivateFormat.TraditionalOpenSSL,
                encryption_algorithm=serialization.NoEncryption(),
            ).decode()
            pub = public_key.public_bytes(
                encoding=serialization.Encoding.PEM,
                format=serialization.PublicFormat.SubjectPublicKeyInfo,
            )
            token = jwt.encode(
                {'iss': 'check_token_unittest', 'sub': 'temp token'},
                pem,
                algorithm='RS512',
            )
            return token, str(pub, encoding='utf-8')

        token, _ = _generate_token()
        with patch('builtins.open', mock_open(read_data='nOTaPublicKey')):
            self.assertRaises(
                SystemExit, opentf.tools.ctl.check_token, token, 'pUbliCKey'
            )

    # versions

    def test_version_1(self):
        mock = MagicMock(return_value='12.34.56')
        with patch('importlib.metadata.version', mock):
            opentf.tools.ctl.get_tools_version()
        mock.assert_called_once()

    # _get_workflow_events

    def test_get_workflow_events_1(self):
        mock_gfp_response = _make_response(200, OBSERVER_FIRST_PAGE_RUNNING_OK)
        mock_gfp_response.headers['Link'] = '<http://example.com/foo>; rel="next"'
        mock_gfp = MagicMock(return_value=mock_gfp_response)
        mock_get_response = Response()
        mock_get_response.status_code = 200
        mock_get_response.json = lambda: OBSERVER_SECOND_PAGE_OK  # pyright: ignore
        mock_get = MagicMock(return_value=mock_get_response)
        with patch('opentf.tools.ctlworkflows._get_first_page', mock_gfp), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch(
            'opentf.tools.ctlworkflows._get', mock_get
        ):
            what = list(_get_workflow_events(UUID_OK, False))
        self.assertEqual(mock_gfp.call_count, 1)
        self.assertEqual(mock_get.call_count, 1)
        self.assertEqual(len(what), 2)

    def test_get_workflow_events_2(self):
        mock_gfp_response = _make_response(200, OBSERVER_FIRST_PAGE_RUNNING_OK)
        mock_gfp = MagicMock(return_value=mock_gfp_response)
        mock_get_response = Response()
        mock_get_response.status_code = 200
        mock_get_response.json = lambda: OBSERVER_FIRST_PAGE_DONE_OK  # pyright: ignore
        mock_get = MagicMock(return_value=mock_get_response)
        with patch('opentf.tools.ctlworkflows._get_first_page', mock_gfp), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch(
            'opentf.tools.ctlworkflows._get', mock_get
        ):
            what = list(_get_workflow_events(UUID_OK, True))
        self.assertEqual(mock_gfp.call_count, 1)
        self.assertEqual(mock_get.call_count, 1)
        self.assertEqual(len(what), 2)
